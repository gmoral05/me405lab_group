'''
@file mcp9808.py
@brief      Brief doc for .py
@details    Detailed doc for .py
@author     Giselle Morales
@date       February 4th, 2020

@package    mcp9808
@brief      Brief doc for the ___ module
@details    Detailed doc for the ___ module
@author     Giselle Morales
@date       February 4th, 2020

'''

# Needs to read 1/sec and print

from pyb import I2C

# class that communicates with I2C interface

class MCP9808:
    '''
    @brief      Allows user to communicate with an MCP9808 temperature sensor using its I2C interface.
    @details    This module takes in
    @author     Giselle Morales
    @date       February 4th, 2020
    '''
    
    def __init__(self, I2C_object, address):
        '''
        @brief  Iniiitializer 
        @details
        @param I2C_object   The I2C object used for communication between MCU and MCP9808
        @param address      The bus address corresponding to the temperature sensor.
        '''
        ## Create the IC2 object
        self.I2C= I2C_object
        
        ## Create the IC2 bus address 
        self.address= address
        
        ## Initialize I2C object
        self.I2C.init(I2C.MASTER, baudrate= 400000)

    def check(self):
        '''
        @brief  Iniiitializer 
        @details
        '''
        # Create the IC2 object
        # # TEST- check if I2C response to address
        I2C.is_ready(0x18)
        # "CHECK" method. Manu_ID= 0110
    def celsius(self, temp):
        '''
        @brief   Converts measured temperature to celsius
        @details
        '''
        # Conversion
        
        
    def fahrenheit(self, temp):
        '''
        @brief   Converts measured temperature to fahrenheit
        @details
        '''
        # Conversion
        
    

    
# Read from I2C to check info
    
if __name__== '__main__':
    # Write test code here
    pass
