'''
@file MotorDriver.py
@brief      Contains the classes and methods for interfacing with the DVR8847 
            motor driver.
@details    This file contains a class for the DVR8847 motor driver, as well
            as an class for a motor object. The motor driver class has 
            methods for enabling and disabling the device, while the motor class
            has a method for controlling the PWM duty cycle of a motor object.
@author     Jordan Kochavi
@author     Giselle Morales
@date       March 7, 2021 Original file.
'''

## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb

class Motor(object):
    '''
    @brief   A class for a motor that can be controlled by a PWM signal.
    @details This class represents a general motor object, which can be 
             controlled in duty-cycle-percentage using a PWM signal. This class
             is utilized by the DVR8847 class. The DVR8847 can control up to
             two Motor objects. This class contains methods to set the speed
             of the motor object, in duty-cycle-percentage, or turn it off.
             The enable() and disable() methods that belong to the DVR8847 
             class affect both motors. However, the off() method that belongs
             to the Motor class will turn off an individual motor object.
    '''
    
    def __init__(self, pin1, pin2, timer, timerChan):
        '''
        @brief   Class constructor for a motor object.
        @details This function constructs a motor object based on its input
                 pins, a timer object, and two timer channel objects for 
                 controlling the PWM duty cycle for the motor.
        @param pin1 A pyboard Pin object.
        @param pin2 A pyboard Pin object.         
        @param timer A pyboard Timer object for outputting a PWM signal.
        @param timerChan A tuple containing two integers, specifying which timer
                 channels to use for the PWM signals.
        '''
        ## Tuple for storing hardware pins for controlling the motor's PWM output.
        self.pins = (pin1, pin2)
        ## Timer object for constructing PWM signal.
        self.tim = timer                                                                         # Assign timer pin
        self.pins[0].init(mode = pyb.Pin.OUT_PP)                                                 # Init IN1 for push-pull output
        self.pins[1].init(mode = pyb.Pin.OUT_PP)                                                 # Init IN2 for push-pull output
        self.tim.init(freq = 20000)                                                              # Init with frequency of 20Khz  
        ## Timer channel object for controlling the pin that produces forward rotation.
        self.fwdChan = self.tim.channel(timerChan[0], mode = pyb.Timer.PWM, pin = self.pins[0])  # Create PWM channel for forward motion
        ## Timer channel object for controlling the pin that produces reverse rotation.
        self.bwdChan = self.tim.channel(timerChan[1], mode = pyb.Timer.PWM, pin = self.pins[1])  # Create PWM channel for reverse motion
    
    def set_duty(self, duty):
        '''
        @brief   Sets the duty cycle for a motor object.
        @details This function receives a duty cycle as an parameter, and outputs
                 a PWM signal to the motor's input pins accordingly.
        @param duty A signed integer containing the duty cycle, in percent, for
                 the motor object. A positive duty cycle indicates forward motion (clockwise),
                 and a negative duty cycle indicates reverse motion (counterclockwise).
        '''
        highChan = self.fwdChan              # Set PWM channel to forward by default
        lowChan = self.bwdChan               # Set off channel to backward by default
        if duty < 0:                         # If the specified duty cycle is less than 0...
            highChan = self.bwdChan          #   Switch to reverse
            lowChan = self.fwdChan           #   Switch to reverse
        duty = abs(duty)                     # Take absolute value after getting direction
        if duty > 100:                       # If duty cycle > 100...
            duty = 100                       #   Then saturate to 100
        highChan.pulse_width_percent(duty)   # Send PWM signal to high channel
        lowChan.pulse_width_percent(0)       # Send off signal to low channel
    
    def off(self):
        '''
        @brief   Turns the motor off.
        @details This method turns the motor off by setting both channels to 0%
                 duty cycle.
        '''
        self.fwdChan.pulse_width_percent(0)  # Set forward channel 0% duty cycle
        self.bwdChan.pulse_width_percent(0)  # Set backward channel 0% duty cycle
        
class DVR8847(object):
    '''
    @brief   A class for the DVR8847 Texas Instruments motor driver chip.
    @details This class represents the hardware functions of the DVR8847
             motor driver chip produced by Texas Instruments. This particular
             motor driver can control up to two motors. Two Motor class objects
             belong to this class. This class contains methods to enable and
             disable the chip, and handle fault detection.
    '''
    
    def __init__(self, nSLEEP_pin, nFault_pin, IN1_pin, IN2_pin, IN3_pin, IN4_pin, timer):
        '''
        @brief   Class constructor for DVR8847 motor driver object.
        @details This function constructs a motor driver object based on a given
                 nSleep pin. This pin is used in the enable() and disable() methods
                 to turn the device on and off.
        @param nSLEEP_pin A pyboard Pin object, which is physically connected
                 to the nSleep pin on the DVR8847 IC.
        @param IN1_pin A pyboard Pin object connected to the motor driver's IN1 pin.
        @param IN2_pin A pyboard Pin object connected to the motor driver's IN2 pin.
        @param IN3_pin A pyboard Pin object connected to the motor driver's IN3 pin.
        @param IN4_pin A pyboard Pin object connected to the motor driver's IN4 pin.
        '''        
        ## The hardware pin attached to the nSleep pin on the motor driver.
        self.nSLEEP = nSLEEP_pin                                                        # Assign sleep pin
        ## The hardware pin attached to the nFault pin on the motor driver.
        self.nFault = nFault_pin                                                        # Assign fault pin 
        self.nFault.init(mode = pyb.Pin.IN, pull = pyb.Pin.PULL_UP)                     # Initialize pin for input
        ## The hardware pin attached to the IN1 pin on the motor driver.
        self.IN1 = IN1_pin                                                              # Assign IN1 pin
        ## The hardware pin attached to the IN2 pin on the motor driver.
        self.IN2 = IN2_pin                                                              # Assign IN2 pin
        ## The hardware pin attached to the IN3 pin on the motor driver.
        self.IN3 = IN3_pin                                                              # Assign IN3 pin
        ## The hardware pin attached to the IN4 pin on the motor driver.
        self.IN4 = IN4_pin                                                              # Assign IN4 pin
        self.nSLEEP.init(mode = pyb.Pin.OUT_PP)                                         # Init sleep for push-pull output
        ## The motor object attached to pins IN1 and IN2.
        self.motor1 = Motor(self.IN1, self.IN2, timer, (1,2))                           # Init motor 1
        ## The motor object attached to pins IN3 and IN4.
        self.motor2 = Motor(self.IN3, self.IN4, timer, (3,4))                           # Init motor 2
        ## The external interrupt object attached to the nFault pin signal.
        self.faultInt = pyb.ExtInt(self.nFault, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.fault_isr) # Attach interrupt to fault pin
        '''
        @brief   Boolean that represents the motor driver's fault status.
        @details When the chip is in fault, this boolean is toggled to True.
                 When the user diagnoses and resolves the fault on the motor 
                 driver chip, this flag should be lowered to return the chip
                 to normal operation.
        '''
        self.fault = False           # Initialize to false
        
    def fault_OK(self):
        '''
        @brief   Function that can be polled to check if the motor driver fault is repaired.
        @details This function checks the status of the nFault pin on the motor driver.
                 Since the nFault pin is active-low, the driver will no longer be in a
                 fault state when the nFault pin returns to high status. 
        @return  Returns True if the motor driver chip is no longer in fault,
                 and returns false if the motor driver chip is still in fault.
        '''
        if self.nFault.value():      # If the nFault pin is high...
            return True              #    Then, the fault is OK, so return True
        else:                        # Else, we're still in fault...
            return False             #    So, return False
        
    def debounce(self, signal, length):
        '''
        @brief   Method to debounce a signal.
        @details This method is used by fault_isr() to debounce the nFault pin.
                 When the chip is enabled, or a motor is turned on, the nFault
                 pin will trigger low as part of the motor driver's response 
                 to the increased current demand. However, the pin will return
                 to normal eventually, so we can treat it as almost a signal
                 noise, and debounce it. This debounce function is setup to
                 look for an active low signal, like the nFault pin.
        @param signal The pin/signal to be debounced.
        @param length How many times the signal should be read.
        @return A boolean. If the signal changes to active low, return True.
                 If it was just momentary, then return False.
        '''
        count = 0                    # Initialize to 0
        for k in range(length):      # Do this length times...
            if not signal.value():   #    If the signal is low... 
                count += 1           #        Add one to the count
        if count > length-3:       # If signal is low for the majority of the readings...
            return True              #    Return True
        else:                        # Otherwise...
            return False             #    Return False
        
    def fault_isr(self, param):
        '''
        @brief   ISR for detecting motor driver fault.
        @details This function is triggered by the falling edge of the pin 
                 attached to the nFault pin on the motor driver chip. Its signal
                 will trigger low when a fault occurs. A fault may occur whenever
                 the chip is enabled, momentarily. However, this isn't a "real" 
                 fault. To avoid detecting a fault triggered by the enable() method,
                 the member, enableFlag serves as a handshake between the ISR and
                 the enable() method. The enable() method raises this flag whenever 
                 it is called, and the ISR lowers it. 
        '''
        if self.debounce(self.nFault, 70): # If a fault is definitively triggered
            self.fault = True              #    Raise the fault flag
            self.disable()                 #    Disable both motors 
        else:                              # Otherwise...
            self.fault = False             #    Keep fault flag lowered
            
    def enable(self):
        '''
        @brief   Method to enable the motor driver object.
        @details This method enables the device by outputting a logic HIGH
                 to the motor driver's enable pin.
        '''
        if not self.fault:           # If the chip is not in fault...
            self.nSLEEP.high()       #     Write nSleep pin HIGH to enable driver
        else:                        # Otherwise...
            self.disable()           #     Turn off the motors and disable the chip 
            
    def disable(self):
        '''
        @brief   Method to disable the motor driver object.
        @details This method disables the device by outputting a logic LOW to 
                 to the motor driver's enable pin.
        '''
        self.nSLEEP.low()            # Write nSleep pin LOW to disable driver

if __name__ == '__main__':                                       # Some test code... 
    PA15 = pyb.Pin(pyb.Pin.board.PA15)                           # For nSleep
    PB2 = pyb.Pin(pyb.Pin.board.PB2)                             # For nFault
    PB4 = pyb.Pin(pyb.Pin.board.PB4)                             # For IN1
    PB5 = pyb.Pin(pyb.Pin.board.PB5)                             # For IN2
    PB0 = pyb.Pin(pyb.Pin.board.PB0)                             # For IN3
    PB1 = pyb.Pin(pyb.Pin.board.PB1)                             # For IN4
    timer3 = pyb.Timer(3)                                        # Create timer object on hardware timer 3 
    motorDriver = DVR8847(PA15, PB2, PB4, PB5, PB0, PB1, timer3) # Create motor driver object
    motorDriver.enable()                                         # Enable the chip
    motorDriver.motor1.set_duty(50)                              # Make a motor spin forward
    motorDriver.motor2.set_duty(-50)                             # Make a motor spin in reverse 
    