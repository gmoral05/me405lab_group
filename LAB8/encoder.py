
'''
@file encoder.py
@brief              This file facilitates the interaction with the DVR8847 motor encoders.
@details            This file contains a class to initialize the encoder mounted onto the 
                    DVR8847 motor drivers. This class allows the user to specify the
                    appropriate pins and timer for the relative encoder. Based on the relative
                    encoder, this code properly fixes the ticks based on the overflow value.
                    The following values are returned: encoder position, difference in previous
                    and current encoder position, RPM, and degrees traveled. The user can also
                    zero the encoder when needed.
                    

@author             Giselle Morales
@author             Jordan Kochavi
@date               March 8, 2021 Original file.

'''
import pyb

class EncoderDriver: 
    '''
    @brief      Returns corrected ticks of the encoder. 
    @details    This class returns the position, change in position and difference 
                between the previous and present encoder position. Uses a 16-bit timer
                which has a maximum period of 65535.
    '''
    
    def __init__(self, pin1, pin2, tim):
            '''
            @brief              Class contructor which creates an Encoder object.
            @details            A constructor for an encoder driver which allows user 
                                to input arbitrary pins and timer and creates two
                                channel objects for the pins. 
            @param pin1         A pyb.Pin object for the encoder object
            @param pin2         A pyb.Pin object for the encoder object
            @param tim          A timer object for the encoder object
            '''
            
            ## Period corresponding to max 16-bit number
            self.overflow= 65535 # 2^16 - 1

            ## Encoder Timer
            self.tim= pyb.Timer(tim, prescaler= 0, period= self.overflow)
            
            ## Two channel objects for relative encoder pins
            self.CH1= self.tim.channel(1, pin = pin1, mode = pyb.Timer.ENC_AB)
            self.CH2= self.tim.channel(2, pin = pin2, mode = pyb.Timer.ENC_AB)
            
            ## Initialize start position
            self.position= 0

            ## Initialize counter based on relative encoder Timer
            self.counter= self.tim.counter()
            
            ## Initialize start position
            self.fixed_delta= 0
            
            ## Create variable to hold previous position of the encoder
            self.previous_counter=0
            
            ## Pulse per cycle for our Motor
            self.PPC= 4
            
            ## Cycles per revolution
            self.CPR= 1000

    def update(self):
        '''
        @brief      Updates current encoder count.
        @details    This function calculates encoder's updated position
                    dependent on encoder period.
        '''
            
        ## Update counter value
        self.counter= self.tim.counter()
            
        ## Delta magnitudes
        self.mag= abs(self.counter- self.previous_counter)
           
        ## Diffrentiate between good and bad deltas.
        if (self.mag < self.overflow/2): #Good delta magnitude
                ## Subtract old count to get self.delta
                self.delta = self.counter-self.previous_counter
                # Converts ticks to degrees
                self.fixed_delta= (self.delta)
                
       ## Diffrentiate between good and bad deltas.     
        elif (self.mag > self.overflow/2):  #Bad delta magnitude
                ## Subtract old count to get self.delta
                self.delta = (self.counter-self.previous_counter)
                
                ## Taking care of negative delta value
                if self.delta < 0:
                    self.fixed_delta= self.delta + (self.overflow)

                ## Taking care of positive delta value
                elif self.delta > 0:
                    self.fixed_delta= self.delta- (self.overflow)
        else:
                    self.fixed_delta= 0
        
        ## Account for previous counter
        self. previous_counter= self.counter
        
         
    def get_position(self):
        '''
        @brief      Returns the most current encoder position
        @details    This function returns updated encoder position without resetting encoder due to overflow
        '''
        
        self.mag= abs(self.counter- self.previous_counter)
        if (self.mag < self.overflow/2): #Good delta magnitude
                
                self.position= self.position + self.get_delta()
                return self.position
                
       ## Diffrentiate between good and bad deltas.     
        elif (self.mag >= self.overflow/2):  #Bad delta magnitude
                ## Subtract old count to get self.delta
                self.delta = (self.counter-self.previous_counter)
                
                ## Taking care of negative delta value
                if self.delta < 0:
                    self.position= self.position + self.get_delta()
                    return self.position

                ## Taking care of positive delta value
                elif self.delta > 0:
                    self.position= self.position + self.get_delta()
                    return self.position
    
        
    def set_position(self, updatedPos):
        '''
        @brief              Sets new encoder position based on user input.
        @param updatedPos   An input to set a position
        '''
        self.position= updatedPos

    def get_delta(self):
        '''
        @brief      Calculates difference between the current encoder counter and previous encoder counter.
        '''
        return self.fixed_delta

    def ticks_RPM(self, interval):
        '''
        @brief              Determines motors angular velocity based on time interval and delta that was calculated.
        @details            This function uses the motor's cycles per revolution to return motor speed during 
                            appropriate run time. 
        @param interval     Time interval in which motor is ran.
        '''
        self.speed= (self.fixed_delta/ (4*1000*4))/(interval/60000)
        return self.speed

    def ticks_deg(self, ticks):
        '''
        @brief              Converts encoder position from ticks to degrees
        @details            This function uses the motor's pulse per cycle and cycles per revolution to return
                            motor rotation in degrees. 
        @param ticks        Total encoder rotation
        '''
        self.deg= ticks*(1/self.PPC)*(1/self.CPR)*(360/1)
        return self.deg


if __name__ == '__main__':
    
    # Create the pin objects used from interfacing with the motor driver
    B6= pyb.Pin.cpu.B6 # First encoder input pin
    B7= pyb.Pin.cpu.B7 # First encoder input pin 
    C6= pyb.Pin.cpu.C6 # Second encoder input pin
    C7= pyb.Pin.cpu.C7
    
    # Create encoder driver
    enc= EncoderDriver(B6, B7, 4)
    enc2= EncoderDriver(C6, C7, 8)
    
    # Get Position
    enc.get_position()
    
    ## Manually turn motor shaft to obtain encoder positions
    while True:
        enc.update()
        enc2.update()
        pos= enc.get_position()
        enc2.get_position()
        enc.ticks_deg(pos)
        print ("Encoder 1 Position: " + str(pos))
        # print ("Encoder 2 Position: " + str(enc2.get_position()))
        print ("Delta 1: " + str(enc.get_delta()))
        # print ("Delta 2: " + str(enc2.get_delta()))
        # print('DEG: ' + str(enc.ticks_deg(pos)))

    
    
    
    