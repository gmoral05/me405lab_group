'''
@file Lab0x08_test.py
@brief      Contains a script that tests the Lab 0x08 motor driver and encoder
            classes.
@details    This test script uses a finite state machine to spin the motors
            and read their encoder counts. It uses the motor driver's fault
            detection to wait for user input until the fault is cleared.
            The user presses the blue button on the Nucleo to return the 
            motor driver to normal operation. The on-board LED indicates to 
            the user when the motor driver chip is no longer in a fault state,
            and the blue button can be pressed to resume operation.
            
            A video of this test script in operation can be viewed in 
            <a href="https://www.youtube.com/watch?v=tzGTvTpxnXo"><b>this Youtube video.</b></a>
            
            A video of this test script for encoder 1 and 2 can be view in
            <a href="https://vimeo.com/522154341"><b>this Vimeo video.</b></a>
            <a href="https://vimeo.com/522154542"><b>and this Vimeo video.</b></a>
            
@author     Jordan Kochavi
@author     Giselle Morales
@date       March 10, 2021 Original file.
'''
## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the motor driver class
from MotorDriver import DVR8847
## @brief Import the encoder driver class
from encoder import EncoderDriver

PA15 = pyb.Pin(pyb.Pin.board.PA15) # For nSleep
PB2 = pyb.Pin(pyb.Pin.board.PB2)   # For nFault
PB4 = pyb.Pin(pyb.Pin.board.PB4)   # For IN1
PB5 = pyb.Pin(pyb.Pin.board.PB5)   # For IN2
PB0 = pyb.Pin(pyb.Pin.board.PB0)   # For IN3
PB1 = pyb.Pin(pyb.Pin.board.PB1)   # For IN4
PB6 = pyb.Pin(pyb.Pin.board.PB6)   # Encoder pin motor 1
PB7 = pyb.Pin(pyb.Pin.board.PB7)   # Encoder pin motor 1
PC6 = pyb.Pin(pyb.Pin.board.PC6)   # Encoder pin motor 2
PC7 = pyb.Pin(pyb.Pin.board.PC7)   # Encoder pin motor 2

## @brief   Initialize the on-board LED that is attached to GPIO pin PA5.
onBoardLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP) # Configure for output, pullup
## @brief   A variable that represents the press status of the blue user button.
pressed = False                    # Init to False
## @brief   The state variable for the FSM.
state = 0                          # Init to 0 

def button_isr(param):             # ISR definition
    global pressed, state          # Reference global variables
    if not pressed and state == 3: # If the button hasn't been pressed yet...
        pressed = True             #     Toggle true
## @brief Set up a hardware interrupt, attached the falling edge of hardware pin PC13. 
extint = pyb.ExtInt(pyb.Pin.board.PC13,   # GPIO pin PC13
         pyb.ExtInt.IRQ_FALLING,          # Interrupt on falling edge
         pyb.Pin.PULL_UP,                 # Activate pullup resistor
         button_isr)                      # Interrupt service routine 

timer3 = pyb.Timer(3)                     # Create timer object for timer hardware 3

motorDriver = DVR8847(PA15, PB2, PB4, PB5, PB0, PB1, timer3) # Create motor driver object
encoder1 = EncoderDriver(PB6, PB7, 4)                        # Create encoder object on timer 4
encoder2 = EncoderDriver(PC6, PC7, 8)                        # Create encoder object on timer 8

print("Starting test program...")

motorDriver.enable()                                         # Enable the chip
motorDriver.motor1.set_duty(-40)                             # Make a motor spin in reverse
motorDriver.motor2.set_duty(40)                              # Make a motor spin forward 

while True:                                                  # Run this forever...
    if state == 0:                                           # State 0 - Init
        state = 1                                            # Transition to state 1
    elif state == 1:                                         # State 1 - Normal operation
        encoder2.update()                                    # Update encoder 2
        position = encoder2.get_position()                   # Get its position, in encoder ticks
        position = encoder2.ticks_deg(position)              # Convert to degrees
        print("Encoder 2 position: "+str(position))          # Print to Putty 
        if motorDriver.fault:                                # If the motor driver is in fault
            print("Fault detected...")                       #    Print error message
            print("LED will light up when fault is resolved")#    Print instructions
            state = 2                                        # Transition to State 2
    elif state == 2:                                         # State 2 - Wait for OK
        if motorDriver.fault_OK():                           # If the fault is resolved...
            onBoardLED.high()                                #    Indicate success with LED
            print("Fault is resolved...")                    #    Print success message
            print("Press blue button to resume operation.")  #    Print instructions
            state = 3                                        #    Transition to State 3  
    elif state == 3:                                         # State 3 - Wait for user input
        if pressed:                                          # If the button is pressed...
            onBoardLED.low()                                 #    Then, turn off the LED
            motorDriver.fault = False                        #    Lower the fault flag to re-enable motor
            motorDriver.enable()                             #    Enable the motor 
            pressed = False                                  #    Lower pressed
            state = 1                                        #    Return to normal operation 
    else:                                                    # If in impossible state...
        pass                                                 #    Do nothing                  