## @file Lab0x04.py
#  A script that compares the ambient temperature to the STM32's internal 
#  temperature.
#
#  This script uses the module, mcp9808.py, to interface with an MCP9808 
#  temperature sensor on a breakout board. This script also obtains the 
#  microcontroller's internal temperature using a built-in ADC function. 
#  Both temperature readings are taken every 60 seconds over an 8 hour period.
#  Readings are stored to a .csv file, which is retrieved from the 
#  microcontroller after data collection is finished.
#
#  @author Jordan Kochavi
#  @author Giselle Morales
#
#  @copyright 2021
#
#  @date February 10, 2021
#
#  @section sec_4a Temperature Plot
#  The plot below contains temperature measurements from the STM32's internal
#  MCU temperature sensor and the mcp9808 module. The test began at around 8:30 AM
#  on Wednesday, 2/10/21, and ran for 8 hours. At around 10:15 AM, I moved the 
#  Nucleo board to a different location in my room, which caused a momentary 
#  steep drop-off at around the 100 minute mark. This is likely due to forced
#  convection over the temperature sensor when I picked it up to move it.
#
#   <img src="https://bitbucket.org/jkochavi/me405_labs/raw/04a048922c0e60c4ca34934a92818701f12b23a4/Lab4/KochaviTemperaturePlot.png" alt="Temperature Graph"> 
#

## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the micropython module used to measure time and implement delay functions.
import utime
## @brief Import the module used to interface with the MCP9808 sensor.
import mcp9808
## @brief Import the I2C module used to connect to the MCP9808 module
from pyb import I2C

## @brief Variable that stores ADC object for reading internal MCU temperature.
adc = pyb.ADCAll(12, 0x70000)

## @brief  Boolean that indicates whether a test is currently being run. 
# @details This variable is toggled high when the user presses the blue button,
#          which begins the test. When the test duration has been exceeded,
#          this variable is toggled false to prevent further measurements from
#          being taken. 
taking_data = False

## @brief   Stores the test start time, which later used to calculate elasped time.
start_time = 0

## @brief   Periodically reset to to count to a minute.
min_time = 0

## @brief   State variable for the FSM.
#  @details This variable stores the current state of the FSM.
state = 0

test_duration = 28800 # 8 hours worth of seconds

## @brief   Initialize the on-board LED that is attached to GPIO pin PA5.
onBoardLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP) # Configure for output, pullup
onBoardLED.low()                                             # Initialize LED to off

## @brief   Callback that toggles the status of the green LED.
#  @details This function is called everytime Timer 1 overflows, which will
#           toggle the status of the built-in LED. This toggling is used to 
#           provide feedback to the user that the program is operating normally.
#           During normal operation, the light will blink slowly, every 1 second.
#           However, after the user has pressed the button, it will blink quickly
#           while the Nucleo transmits its data back to the PC. When it is done
#           transmitting, then the LED returns to blinking slowly
# @param timer An unused parameter.
# @return This function does not return anything, as it is a callback function.
def LEDcallBack(timer):                     # Function definition
    global taking_data                      # Reference global variable
    if taking_data:                         # If taking data...
        if onBoardLED.value():              # If the LED was already on...
            onBoardLED.low()                #    Then turn it off 
        else:                               # Otherwise...
            onBoardLED.high()               # Turn it on
    else:                                   # Otherwise...
        onBoardLED.low()                    # Leave the LED off

## @brief  Create a timer object for timer hardware 1
tim1 = pyb.Timer(1, freq=1)             # Timer 1 running at 1 Hz
tim1.callback(LEDcallBack)              # Assign callback function accordingly

## @brief   An interrupt service routine for the on-board button.
#  @details This ISR is triggered on the falling edge of GPIO pin PC13, which
#           is attached to the on-board button. The user presses the button
#           to begin taking data.  
#  @param param An unused parameter.
#  @return This function does not return anything, as it is a callback function.
def button_isr(param):                  # Define ISR
    global taking_data, start_time      # Reference global variables
    if not taking_data:                 # If not already taking data...
        taking_data = True              #    Then, start the test,
        start_time = utime.ticks_ms()   #    Record start time

## @brief Set up a hardware interrupt, attached the falling edge of hardware pin PC13. 
extint = pyb.ExtInt(pyb.Pin.board.PC13, # GPIO pin PC13
             pyb.ExtInt.IRQ_FALLING,    # Interrupt on falling edge
             pyb.Pin.PULL_UP,           # Activate pullup resistor
             button_isr)                # Interrupt service routine

i2c = I2C(1)                            # Create I2C object on channel 1
tempSensor = mcp9808.MCP9808(i2c)       # Create sensor object

## @brief   A function that checks elapsed time.
#  @details This function has multiple uses. It is called once per minute, 
#           and returns true if a minute has passed. This indicates to the FSM
#           that a new temperature measurement needs to be taken. This function
#           is also called after each temperature measurement, to check if the 
#           test has exceeded its maximum duration.
#  @param start An integer containing the ticks_ms value at the measurement start.
#  @param duration An integer that represents the number of seconds the duration
#           should last.
#  @return  A boolean. This function returns true if the specified duration has
#           been exceeded, and returns false if it has not been exceeded. 
def checkTime(start, duration):               # Function definition
    timeDone = False                          # Initialize to false
    currentTime = utime.ticks_ms()            # Retrieve current time
    elapsed = (currentTime - start)/1000      # Calculate elapsed time, in seconds
    if elapsed > duration:                    # If we have acheived desired duration...
        timeDone = True                       #    Then toggle true
    return timeDone                           # Return timeDone boolean

while True:                                                                    # Run this code n a forever loop
    try:                                                                       # If there are no keyboard interruptions...
        if state == 0:                                                         # State 0 - Init
            global min_count                                                   # Reference global variable
            ## @brief Variable that counts elapsed minutes of test.            #
            min_count = 0                                                      # Initialize to zero 
            print("Press blue button to begin test...")                        # Print introduction message to user
            adc.read_vref()                                                    # Read supply voltage to calibrate internal temp
            if tempSensor.check():                                             # If sensor connected...
                print("Sensor ID connected and matches.")                      #     Print success message
                print("Test Duration = "+str(test_duration)+" [s]")            #     Print test duration 
                state = 1                                                      #     Transition to state 1 
        elif state == 1:                                                       # State 1 - Wait for press
            if taking_data:                                                    # If the user has begun the test...
                min_time = utime.ticks_ms()                                    #     Then, start minute counter
                print("Starting data recording...")                            #     Tell the user that the test has begun
                with open("dataLogger.csv","w") as a_file:                     # Create CSV file
                    a_file.write("Time [min], STM32 Temp [C], Ambient Temp [C]\r")# Label columns        
                state = 2                                                      #     Transition to state 2
        elif state == 2:                                                       # State 2 - Count to one minute 
            if checkTime(min_time, 60):                                        # If one minute has passed...   
                state = 3                                                      #     Transition to state 3 to collect data
        elif state == 3:                                                       # State 3 - Take data 
            internalTemp = adc.read_core_temp()                                # Read the internal MCU temp 
            ambientTemp = tempSensor.celsius()                                 # Read ambient temperature from sensor
            with open("dataLogger.csv","a") as a_file:                         # Open CSV file in append format
                a_file.write(str(min_count)+","+str(internalTemp)+","+str(ambientTemp)+"\r")# Append data to next line in file 
            min_count += 1                                                     # Increment minute count
            if checkTime(start_time, test_duration):                           # If test has exceeded maximum duration...
                print("Done!")                                                 #      Then, print success message to user
                state = 4                                                      #      Transition to state 4 
            else:                                                              # If the test is still going...
                min_time = utime.ticks_ms()                                    #      Then, store current time
                state = 2                                                      #      Return to state 2 until next data recording 
        elif state == 4:                                                       # State 4 - Reset
            taking_data = False                                                # Reset boolean to false, no longer taking data
            min_count = 0                                                      # Reset to zero
            print("Press button again to start another test")                  # Print instruction 
            print("Or, press cntl+c to exit.")                                 # Print instruction
            state = 1                                                          # Return to state 1
        else:                                                                  # Forbidden state/error occurs...
            pass                                                               #     Do nothing...
    except KeyboardInterrupt:                                                  # If the user enters a keyboard interrupt...
        break                                                                  #     Then, break out of the loop
    
print("Exiting program...")
print("Data stored to dataLogger.csv")
print("To retrieve:")
print("*** Open Anaconda Prompt")
print("*** Navigate to a directory of your choice")
print("*** Type ampy --port <device port> get dataLogger.csv savedData.csv")
print("*** savedData.csv will be created at PC's local directory")