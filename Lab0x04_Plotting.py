## @file Lab0x04_Plotting.py
#  A script that plots the spreadsheet generated by the main.py file for Lab0x04.
#
#  After a user runs a test of the MCP9808 temperature module using the main.py
#  script for Lab0x04, they should run this script in Spyder to read and plot
#  data from the spreadsheet titled, 'savedData.csv'. This script uses the csv
#  python module to create a reader object, and append each element of each row
#  to a different column. For more information, visit the Stack Overflow forum
#  post at: https://stackoverflow.com/questions/37686105/python-splitting-data-as-columns-in-csv-file
#
#  @author Jordan Kochavi
#  @author Giselle Morales
#
#  @copyright 2021
#
#  @date February 10, 2021
#
#

## @brief   Module to plot to make custom graphs and plots.
import matplotlib.pyplot as plt

## @brief   Module used to check whether a file exists or not.
from os import path

## @brief   Module used to read and parse CSV file.
import csv

## @brief   Variable to store measured time domain, in minutes.
time = []

## @brief   Variable to store measured MCU temperature, in Celsius.
internalTemp = []

## @brief   Variable to store measured ambient temperature, in Celsius.
ambientTemp = []

## @brief   Function that converts each element of a list to an float.
# @details  This function receives a list of strings as a parameter, and uses
#           a for-loop to convert each item in the list to a float. This function
#           must be called after all words and letters are removed from the list.
#           This function is only useful for converting a list of numbers that
#           are in string format, to floats.
# @param listVar A list of numbers in string format.
# @return A converted list.
def strToFlt(listVar):                                   # Function definition
    for k in range(len(listVar)):                        # For each element in the list...
        listVar[k] = float(listVar[k])                   # Convert to float
    return listVar                                       # Return the list

if path.exists("savedData.csv"):                         # If a valid data file exists...
    with open("savedData.csv", "r") as f:                # Open it in read format
        csvReader = csv.reader(f, delimiter = ",")       # Create a CSV reader object
        for row in csvReader:                            # For each row of the spreadsheet...
            time.append(row[0])                          # Append first column to time list
            internalTemp.append(row[1])                  # Append second column to internal list   
            ambientTemp.append(row[2])                   # Append third column to ambient list
    time.pop(0)                                          # Remove column label from time list
    internalTemp.pop(0)                                  # Remove column label from internal list
    ambientTemp.pop(0)                                   # Remove column label from ambient list
    time = strToFlt(time)                                # Convert time list to float
    internalTemp = strToFlt(internalTemp)                # Convert internal list to float
    ambientTemp = strToFlt(ambientTemp)                  # Convert ambient list to float
    plt.plot(time, ambientTemp, label="Ambient")         # Plot ambient temp vs. time
    plt.plot(time, internalTemp,label="MCU Internal")    # Plot internal temp vs. time 
    plt.title("Ambient and MCU Temperature vs. Time")    # Apply title
    plt.xlabel("Time [minutes]")                         # Label the x-axis
    plt.ylabel("Temperature [C]")                        # Label the y-axis
    plt.legend()                                         # Add legent
    plt.show()                                           # Display the plot in Spyder's plot window
else:                                                    # If no valid data file exists...
    print("File named, savedData.csv, is not found.")    #     Print error message and exit
    print("Please check file directories or run another test to generate file.")