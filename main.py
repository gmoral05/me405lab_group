'''
@file main.py
@brief      Obtains temperature from STM32 and MCP9808 sensor.
@details    This script will take temperature readings from both the STM32 and 
            a MCP9808 sensor. A module called mcp9808 is called into this script.
            This script runs for a full 8 hours taking temperature readings every
            minute. The total rows of data shall reach 480 if the script isn't
            interrupted by a keyboard. In either case, this script will close
            the file and provides it to the user in the form .csv. 
@author     Giselle Morales
@date       February 10th, 2020

@image html TemperatureData_Giselle.png

'''
import pyb
# Import created module for mcp9808 sensor
import mcp9808
import utime

## Create the I2C constructor
I2C_object= pyb.I2C(1)
## Create address parameter
address= 0x18
## Create object for temp sensor
mcp= mcp9808.MCP9808(I2C_object, address)
## Object for measuring board temp
adcall= pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
## Read MCU reference voltage before reading temperature
adcall.read_vref()
## Check mcp address and manufacturer ID
mcp.check() 
## Start run count 
run= 0
## Start time
start_tim= utime.ticks_ms()

# Create .csv file to store temperature readings from STM34
with open ('TemperatureData.csv', 'w') as file:
    file.write('Time [sec], MCU Temperature[deg C], MCP Temperature [deg C]\n')
    while True:
        try:
            ## Update elapsed time
            curr_tim= utime.ticks_ms()
            print(run) # debugging
            utime.sleep(60) #sleep for 60 seconds
            ## Read the temp of the MCU
            inter_temp = adcall.read_core_temp()#loop to keep updating
            print ('MCU temp:' + str(inter_temp)) #debugging
            ## Read MCP9808 temperature
            MCP_temp= mcp.celsius()
            print ('MCP temp:' + str(MCP_temp)) #debugging
            ## Time for data collection
            data_tim= (curr_tim- start_tim)/1000
            file.write('{:.0f}, {:.2f}, {:.2f}\n'.format(data_tim, inter_temp, MCP_temp))
            run += 1
            
            # Break out of the loop if 8 hours have passed
            if run == 480:
                file.close()
                break

        except KeyboardInterrupt:
            # Close file but keeps data
            file.close()
            break

print ('Done. The file has been closed automatically.')
