## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the motor driver class
from MotorDriver import DVR8847
## @brief Import the encoder driver class
from encoder import EncoderDriver

from touchPanel import touch_panel

from controller import fullState

import utime

PA15 = pyb.Pin(pyb.Pin.board.PA15) # For nSleep
PB2 = pyb.Pin(pyb.Pin.board.PB2)   # For nFault
PB4 = pyb.Pin(pyb.Pin.board.PB4)   # For IN1
PB5 = pyb.Pin(pyb.Pin.board.PB5)   # For IN2
PB0 = pyb.Pin(pyb.Pin.board.PB0)   # For IN3
PB1 = pyb.Pin(pyb.Pin.board.PB1)   # For IN4
PB6 = pyb.Pin(pyb.Pin.board.PB6)   # Encoder pin motor 1
PB7 = pyb.Pin(pyb.Pin.board.PB7)   # Encoder pin motor 1
PC6 = pyb.Pin(pyb.Pin.board.PC6)   # Encoder pin motor 2
PC7 = pyb.Pin(pyb.Pin.board.PC7)   # Encoder pin motor 2
PA7 = pyb.Pin(pyb.Pin.board.PA7)   # X+
PA1 = pyb.Pin(pyb.Pin.board.PA1)   # X- 
PA6 = pyb.Pin(pyb.Pin.board.PA6)   # Y+
PA0 = pyb.Pin(pyb.Pin.board.PA0)   # Y-

R = 2.21 # ohms
Vdc = 12 # Volts
Kt = 13.8 # mN-m/A

#K1 = [-8, -0.28, -100, -50]

#K2 = [-15, -0.28, -100, -30]

K1 = [-2, -0.5, -50, -13]
K2 = [-2, -0.5, -30, -10]

timer3 = pyb.Timer(3)                     # Create timer object for timer hardware 3
dimensions = (176,100)
center = (1966, 2068) # Obtained from calibration script
resolution = (21,34)  # Obtained from calibration script

panel = touch_panel((PA7, PA1, PA6, PA0),dimensions,center,resolution)
motorDriver = DVR8847(PA15, PB2, PB4, PB5, PB0, PB1, timer3) # Create motor driver object
encoder1 = EncoderDriver(PB6, PB7, 4)                        # Create encoder object on timer 4
encoder2 = EncoderDriver(PC6, PC7, 8)                        # Create encoder object on timer 8

system = fullState(panel, motorDriver, encoder1, encoder2, (R, Vdc, Kt))

def waitingDone(time):
    time[1] = utime.ticks_ms()
    if (time[1] - time[0]) > 50:
        return True
    else:
        return False

time = [0,0]
state = 0
while True:
    if state == 0:
        system.setKmatrix(1,K1)
        system.setKmatrix(2,K2)
        system.driver.enable()
        utime.sleep_ms(20)
        state = 1
        
    elif state == 1:
        if waitingDone(time):
            time[0] = time[1]
            state = 2
            
    elif state == 2:
        system.update()
        #system.testValues()
        state = 1

