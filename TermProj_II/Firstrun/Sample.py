# -*- coding: utf-8 -*-
"""
@file sample.py
@brief              This file calibrates the platform and balances it.
@details            
                    

@author             Giselle Morales
@author             Jordan Kochavi
@date               March 15, 2021 Original file.
"""


## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the motor driver class
from MotorDriver import DVR8847
## @brief Import the encoder driver class
from encoder import EncoderDriver

from touch import touch
from controller import fullState
import utime


class TaskDataCollection:

    '''
    Data Collection task.
    '''

    ## Initialization state
    S0_INIT                = 0
    
    ## Wait for Data and Read
    S1_SET_ORIGIN          = 1

    ## Wait for Data collection
    S2_CALLIBRATE          = 2

    ## Wait for Data collection
    S3_CONTROL             = 3

    def __init__(self, interval, encoder1, encoder2, MotorDriver, panel, controller):
        '''
        Creates a TaskDataCollection object.
        @param interval         A number to specify interval between tasks
        @param encoder          An object that is created from an imported encoderdriver class
        @param encoder          An object that is created from an imported motordriver class
        '''

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## The timestamp for the first iteration
        self.start_time = int(utime.ticks_us())# The number of miliseconds since Jan 1. 1970
        ## The interval of time, in miliseconds, between runs of the task
        # self.inte= 4000*1e6
        # self.interval = int(inte)
        self.R = 2.21 # ohms
        self.Vdc = 12 # Volts
        self.Kt = 13.8 # mN-m/A
        self.interval= interval
        self.encoder1= encoder1
        self.encoder2= encoder2
        self.MotorDriver= MotorDriver
        self.panel= panel
        self.controller= controller
        self.K = [-9.09, -3.99, -3.28, -0.28] # x, theta, x_dot, theta_dot
        ## The "timestamp" for when to run the task next
        # self.next_time = int(utime.ticks_add(self.start_time, self.interval))   

        # # Check Z to determine contact
        # values= panel.read()

# States
# 1) Calibrate platform- here we set the encoders to zero to ensure change in movements are calculated correctly
# 2) Read initial platform angle changes. These changes are read from the encoder and converted to degrees. The change (delta)
# is divided by the relative interval the script is runnning at to get velocity. 
# 3) The controller is then called in to calculate the output motor torque and convert it to duty cycle
# 4) The platforms moves as appropriate 
# 5) Add a loop for continual ball control 

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        # self.curr_time = int(utime.ticks_us())
        # if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
        if(self.state == self.S0_INIT):
                # Make sure touch panel and encoders exist
                if panel.read == False:
                    print ('No touch panel detected: ')
                    self.transitionTo(self.S0_INIT)
                else:
                    self.transitionTo(self.S1_SET_ORIGIN) 
        
        elif(self.state == self.S1_SET_ORIGIN):
                # Run State 1 Code
                # Balace the board- starting position
                posx= 0
                encoder1.set_position(posx)
                
                posy= 0
                encoder2.set_position(posy)
                
                # Obtain touch panel read out
                initi_ball= panel.read()
                # print (initi_ball)
                if initi_ball== False:
                    print ('No readout available')
                    self.transitionTo(self.S1_SET_ORIGIN)
                else:
                    self.transitionTo(self.S2_CALLIBRATE)
                    
        elif(self.state == self.S2_CALLIBRATE):
                # Update/read encoder changes based on ball movements. Get encoder rotation which translates to x/y movements.
                encoder1.update()               # X position
                x_move= encoder1.get_position()
                x= encoder1.ticks_deg(x_move)
                x_vel= encoder1.get_delta()/ (interval)  #interval in microseconds

                encoder2.update()               # Y position
                y_move= encoder2.get_position()
                y= encoder2.ticks_deg(y_move)
                y_vel= encoder2.get_delta()/ (interval)
                
                self.motion= [x, x_vel, y, y_vel]
                # print(str(self.motion))
                if self.motion == False:
                    print('Nothing detected on touch panel')
                    self.transitionTo(self.S2_CALLIBRATE)
                else:
                    self.transitionTo(self.S3_CONTROL)

        elif(self.state == self.S3_CONTROL):
                    # Control platform based on platform and ball change in position. Output here is torque which is then converted
                    # to duty cycle to run motor as appropriate.
                    Tx= (self.K[0]* self.motion[0]) + (self.K[1]*self.motion[1])
                    Ty= (self.K[2]* self.motion[2]) + (self.K[3]*self.motion[3])
                    # Convert these torques to duty cycle using controller
                    # duty= controller.duty(Tx, Ty)
                    Dx= ((self.R/ (self.Vdc*self.Kt))*Tx)*100 #percentage
                    Dy= ((self.R/ (self.Vdc*self.Kt))*Ty)*100 #percentage
                    duty=[Dx, Dy]
                
                    print (duty)                                    # Check conversion to duty is corret
                    motorDriver.enable() 
                    motorDriver.motor1.set_duty(duty[0]) # Set to calculated duty
                    motorDriver.motor2.set_duty(duty[1]) # Set to calculated duty
                    self.transitionTo(self.S2_CALLIBRATE)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

## The interval of time, in miliseconds, between runs of the task
inte= 4000*1e6
interval = int(inte)

PA15 = pyb.Pin(pyb.Pin.board.PA15) # For nSleep
PB2 = pyb.Pin(pyb.Pin.board.PB2)   # For nFault
PB4 = pyb.Pin(pyb.Pin.board.PB4)   # For IN1
PB5 = pyb.Pin(pyb.Pin.board.PB5)   # For IN2
PB0 = pyb.Pin(pyb.Pin.board.PB0)   # For IN3
PB1 = pyb.Pin(pyb.Pin.board.PB1)   # For IN4
PB6 = pyb.Pin(pyb.Pin.board.PB6)   # Encoder pin motor 1
PB7 = pyb.Pin(pyb.Pin.board.PB7)   # Encoder pin motor 1
PC6 = pyb.Pin(pyb.Pin.board.PC6)   # Encoder pin motor 2
PC7 = pyb.Pin(pyb.Pin.board.PC7)   # Encoder pin motor 2
ym= pyb.Pin.cpu.A0
xm= pyb.Pin.cpu.A1
yp= pyb.Pin.cpu.A6
xp= pyb.Pin.cpu.A7

# Set up panel length and width (m), and center point
Lx= 0.176
Ly= 0.100
cen_x= None
cen_y= None
timer3 = pyb.Timer(3)                     # Create timer object for timer hardware 3      
R = 2.21 # ohms
Vdc = 12 # Volts
Kt = 13.8 # mN-m/A
        
panel= touch(ym,xm,yp,xp,Lx, Ly, cen_x, cen_y)               # Initialize touch panel
motorDriver = DVR8847(PA15, PB2, PB4, PB5, PB0,PB1, timer3) # Create motor driver object
encoder1 = EncoderDriver(PB6, PB7, 4)                        # Create encoder object on timer 4, controls x motion
encoder2 = EncoderDriver(PC6, PC7, 8)                        # Create encoder object on timer 8, controls y motion
controller= (panel, motorDriver, encoder1, encoder2, (R, Vdc,Kt))
  
## Task, works with serial port
task1 = TaskDataCollection(interval, encoder1, encoder2, motorDriver, panel, controller)      
while True:
        task1.run()
