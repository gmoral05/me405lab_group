'''
@file touch.py
@brief      This file contains the methods to interface with a TFT080-1 touch screen panel.
@details    This script contains a class and methods for the TFT080 touch panel.
            This touch class initiates pins. established the panel's center, length
            and width, and returns the position of an object in mm. The screens 
            total length in the x direction is 176 mm and 99.36 mm in the y direction.
            Since the origin is set in the middle of the touch screen, values go negative
            or positive relative to this point. A bolean False is returned if the touch
            screen no being touched, and a boolean True if there is contact.
@author     Giselle Morales
@date       March 3rd, 2021

'''

import pyb
import utime
class touch:
    '''
    @brief      Obtains x and y positions for a touch panel as well as a boolean 
                representing if something is in contact with the touch panel.
    @details    This module sets up the appropriate pins for a TFT080-1 touch 
                panel. Based on the length and width of the panel, this module 
                correctly calculates the center of the touch panel. In return,
                position values are normalized and returned to the user.
    '''
    
    def __init__(self,ym ,xm ,yp ,xp ,Lx ,Ly ,cen_x ,cen_y):
        '''
        @brief          Class object initilizer
        @param ym       A pyb.Pin object for the ym pin
        @param xm       A pyb.Pin object for the xm pin
        @param yp       A pyb.Pin object for the yp pin
        @param xp       A pyb.Pin object for the xp pin
        @param Lx       Length along x direction
        @param Ly       Length along y direction
        @param cen_x    X point for origin
        @param cen_y    Y point for origin
        '''
        ## Create ym Pin object
        self.ym= ym
        ## Create xm Pin object
        self.xm= xm
        ## Create yp Pin object
        self.yp= yp
        ## Create xp Pin object
        self.xp= xp
        ## Create Lx object correlated with length of board
        self.Lx= Lx
        ## Create Ly object correlated with width of board
        self.Ly= Ly
        ## Create x point for origin
        self.cen_x= cen_x
        ## Create y point for origin
        self.cen_y= cen_y
        
        ## Calculate center of screen automatically if no value is entered
        if cen_x== None:
            self.cen_x= Lx/2
        else:
            self.cen_x= cen_x
        
        if cen_y== None:
            self.cen_y= Ly/2
        else:
            self.cen_y= cen_y
            
        ## Max value obtained for my touch screen was 4095. Used to normalize values.
        self.div= 4095
        
    def x(self):
        '''
        @brief      Reads x direction values from touch panel.
        @details    Sets up board pins to entable voltage readings from touch panel.
                    In turn, these values are converted to mm and outputted.
        @return     Normalized x position of the baord based on board metrics
        '''
        ## Create Pins
        xp= pyb.Pin(self.xp)
        xm= pyb.Pin(self.xm)
        ## Initialize pins
        xp.init(mode=pyb.Pin.OUT_PP, value= 1)
        xm.init(mode=pyb.Pin.OUT_PP, value=0)
        ## Set pins yp and ym to ADC
        yp= pyb.ADC(self.yp)
        ym= pyb.ADC(self.ym)
              
        ## Collect reading
        valx= ym.read()
         
        ## Convert to m
        meas_x= valx/ self.div*self.Lx- self.cen_x
        
        ## Return x measurment in mm
        return meas_x*1000
    
    def y(self):
        '''
        @brief      Reads y direction values from touch panel.
        @details    Sets up board pins to enable voltage readings from touch panel.
                    In turn, these values are converted to mm and outputted.
        @return     Normalized y position of the baord based on board metrics
        '''
        ## Create Pins
        yp= pyb.Pin(self.yp)
        ym= pyb.Pin(self.ym)
        ## Initialize pins
        yp.init(mode=pyb.Pin.OUT_PP, value= 1)
        ym.init(mode=pyb.Pin.OUT_PP, value=0)
        ## Set pins yp and ym to ADC
        xp= pyb.ADC(self.xp)
        xm= pyb.ADC(self.xm)
                
        ## Collect reading
        valy= xm.read()
        
        ## Convert to m
        meas_y= valy/ self.div*self.Ly-self.cen_y
        
        ## Return y measurment in mm
        return meas_y*1000
    
    
    def z(self):
        '''
        @brief      Sets True/False boolean based on touch panel contact.
        @details    Sets up board pins to enable voltage readings from touch panel.
                    If the value is less than my board error value (5), a boolean False
                    is returned meaning no contact
        @return     Boolean establishing contact with touch screen (True) or (False) for no contact
        '''
        ## Create Pins
        yp= pyb.Pin(self.yp)
        xm= pyb.Pin(self.xm)
        yp.init(mode=pyb.Pin.OUT_PP, value= 1)
        xm.init(mode=pyb.Pin.OUT_PP, value= 0)
        xp= pyb.ADC(self.xp)
        ym= pyb.ADC(self.ym)
        
        ## Collect reading
        valz= xp.read()
    
        ## Return boolean
        if valz> 5: #Greatest value obtained on my screen without touching it
            return True
        else:
            return False
    
    def read(self):
        '''
        @brief      Returns tuple
        @details    Returns converted output values from x,y, and z methods 
        @return     A tuple of collected values
        '''
        return (self.x(), self.y(), self. z())   
    
    
    
if __name__== '__main__':
    # Call out pins
    ym= pyb.Pin.cpu.A0
    xm= pyb.Pin.cpu.A1
    yp= pyb.Pin.cpu.A6
    xp= pyb.Pin.cpu.A7
    
    
    # Set up panel length and width (m), and center point
    Lx= 0.176
    Ly= 0.100
    cen_x= None
    cen_y= None
    # Initialize touch panel
    panel= touch(ym,xm,yp,xp,Lx, Ly, cen_x, cen_y)
    # Check Z to determine contact
    values= panel.read()
    
    
    ## Scaning x, y, and z
    while True: 
        try:
            start_flag= True
            starttim= utime.ticks_us()
            values= panel.read()
            endtim= utime.ticks_diff(utime.ticks_us(), starttim)
            if start_flag == True:
                if values[2]== False:
                    flag= False
                    print ('No Contact')
                else:
                    flag= True
                    start_flag= False

            if flag== True:
                endtim= utime.ticks_diff(utime.ticks_us(), starttim)
                print ('Time (us): '+ str(endtim) + '\n'+ 'X (mm): ' + str(values[0])+ '\n'+ 'Y (mm): '+ str (values[1])+ '\n'+ 'Z: '+ str (values[2])+ '\n')
                
            # Add delay
            utime.sleep(0.26)
            start_flag= True
            
        except KeyboardInterrupt:
            print ('Aborted')
            break
        

    # ## Testing tuple scan time 
    # while True:
    #     try:
    #         for n in range (10):
    #             starttim= utime.ticks_us()
    #             xyzscan= panel.read()
    #             endtim= utime.ticks_us()
    #             tottim= utime.ticks_diff(utime.ticks_us(), starttim)
    #             print ('x reading: '+ str(xyzscan)+ ' Time (us): ' + str(tottim))
            
            
    #     except KeyboardInterrupt:
    #         print ('Aborted')
    #         break


    # ## Testing x scan time 
    # while True:
    #     try:
    #         for n in range (10):
    #             starttim= utime.ticks_us()
    #             xscan= panel.x()
    #             endtim= utime.ticks_us()
    #             tottim= utime.ticks_diff(utime.ticks_us(), starttim)
    #             print ('x reading: '+ str(xscan)+ ' Time (us): ' + str(tottim))
            
            
    #     except KeyboardInterrupt:
    #         print ('Aborted')
    #         break

    # ## Testing y scan time 
    # while True:
    #     try:
    #         for n in range (10):
    #             starttim= utime.ticks_us()
    #             yscan= panel.y()
    #             endtim= utime.ticks_us()
    #             tottim= utime.ticks_diff(utime.ticks_us(), starttim)
    #             print ('y reading: '+ str(yscan)+ ' Time (us): ' + str(tottim))
            
    #     except KeyboardInterrupt:
    #         print ('Aborted')
    #         break

    # ## Testing z scan time 
    # while True:
    #     try:
    #         for n in range (10):
    #             starttim= utime.ticks_us()
    #             zscan= panel.z()
    #             endtim= utime.ticks_us()
    #             tottim= utime.ticks_diff(utime.ticks_us(), starttim)
    #             print ('z reading: '+ str(zscan)+ ' Time (us): ' + str(tottim))
            
    #     except KeyboardInterrupt:
    #         print ('Aborted')
    #         break
    
 
    
        