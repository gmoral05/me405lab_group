'''
@file controller.py
@brief      Contains the class and methods for implementing a full-state 
            feedback controller.
@details    This class works in conjunction with other sensor and feedback
            modules to implement a full-state feedback controller. The class
            is specific to the ball and platform assembly of the ME 405 term
            project, and uses the DVR8447 motor driver class, the EncoderDriver
            class, and the touch_panel class. 
@author     Jordan Kochavi
@author     Giselle Morales
@date       March 15, 2021 Original file.
'''

from touchPanel import touch_panel
from MotorDriver import DVR8847
from encoder import EncoderDriver
import pyb
import utime

class fullState:
    def __init__(self, panel, motorDriver, encoder1, encoder2, motorParams):
        self.ball_sensor = panel
        self.motor1_sensor = encoder1
        self.motor2_sensor = encoder2
        self.driver = motorDriver
        
        self.driver.motor1.setParameters(motorParams[0], motorParams[1], motorParams[2])
        self.driver.motor2.setParameters(motorParams[0], motorParams[1], motorParams[2])
        
        self.ballTime = [0,0]     # 0 index-> previous, 1 index-> current
        self.ballPos = [0,0]  # [x, y]
        self.prevBallPos = self.ballPos # Position at last time increment
        self.ballVel = [0,0]
        
        self.motorTime = [0,0]
        self.motorPos = [0,0] # [motor 1, motor 2]
        self.prevMotorPos = self.motorPos
        self.motorVels = [0,0] # [motor 1, motor 2]
        
    def getBallPos(self):
        if self.ball_sensor.readZ():
            self.ballPos = self.ball_sensor.read()
        else:
            self.ballPos = [0,0]
        #return self.ballPos

    def getBallVel(self):
        self.ballTime[1] = utime.ticks_us()
        self.ballVel[0] = 1000000*(self.ballPos[0] - self.prevBallPos[0])/(self.ballTime[1] - self.ballTime[0]) # delta X / delta t
        self.ballVel[1] = 1000000*(self.ballPos[1] - self.prevBallPos[1])/(self.ballTime[1] - self.ballTime[0]) # detla Y / delta t
        
        self.prevBallPos = self.ballPos
        self.ballTime[0] = self.ballTime[1]
    
        #return self.ballVel
    
    def getMotorAngles(self):
        self.motor1_sensor.update()
        self.motor2_sensor.update()
        position1 = self.motor1_sensor.get_position()
        position1 = self.motor1_sensor.ticks_deg(position1)*3.1415926/180
        
        position2 = self.motor2_sensor.get_position()
        position2 = self.motor2_sensor.ticks_deg(position2)*3.1415926/180
        
        self.motorPos = [position1, position2] # radians

        #return self.motorPos

    def getMotorVels(self):
        self.motorTime[1] = utime.ticks_us()
        self.motorVels[0] = 1000000*(self.motorPos[0] - self.prevMotorPos[0])/(self.motorTime[1] - self.motorTime[0]) # delta X / delta t
        self.motorVels[1] = 1000000*(self.motorPos[1] - self.prevMotorPos[1])/(self.motorTime[1] - self.motorTime[0]) # detla Y / delta t
        
        self.prevMotorPos = self.motorPos
        self.motorTime[0] = self.motorTime[1]     
        
        #return self.motorVels # radians per second
    
        
    def setKmatrix(self,k):
        self.K = k
    
    def update(self):        
        self.getBallPos()
        self.getBallVel()
        self.getMotorAngles()
        self.getMotorVels()
        
        if not self.driver.fault:
            Tx = self.K[0]*self.ballPos[0]+self.K[1]*self.motorPos[1]+self.K[2]*self.ballVel[0]+self.K[3]*self.motorVels[1]
            dutyX = -1*Tx*self.driver.motor1.gain
            
            print("DutyX: "+str(dutyX))
            
            dutyX += 30
            self.driver.motor1.set_duty(dutyX)
            
            Ty = self.K[0]*self.ballPos[1]+self.K[1]*self.motorPos[0]+self.K[2]*self.ballVel[1]+self.K[3]*self.motorVels[0]
            dutyY = -1*Ty*self.driver.motor2.gain
            
            print("DutyY: "+str(dutyY))
            
            dutyY += 30
            self.driver.motor2.set_duty(dutyY)
   
        
        
        