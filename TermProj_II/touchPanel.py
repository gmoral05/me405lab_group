'''
@file touchPanel.py
@brief      This file contains the methods to interface with a 4-wire
            touch panel object.
@details    This script contains a class and methods for a standard 4-wire 
            resistive touch panel. This is a generic touch panel class that
            can be used for any-sized 4-wire resistive touch panel. Its methods
            compensate for the touch panel size, and measure the touched-coordinate
            relative to a user-specified center. 
@author     Jordan Kochavi
@date       March 3, 2021 Original file.

@package    touchPanel
@brief      A module for reading a 4-wire resistive touch panel.
@details    This module reads the X and Y coordinate of the touched location
            on a 4-wire resitive touch panel. It also returns a Z value, which
            is a boolean flag raised when the panel is touched. 
@author     Jordan Kochavi
@date       March 3, 2021 Original file.

'''

## Import the pyb module.
import pyb
## Import module for time delays and measurements.
import utime

class touch_panel:
    '''
    @brief      Allows user to read the pressed coordinates, in millimeters, from a 4-wire touch panel.
    @details    This class contains methods for reading data from a standard 4-wire 
                resistive touch panel. This is a generic touch panel class that
                can be used for any-sized 4-wire resistive touch panel. Its methods
                compensate for the touch panel size, and measure the touched-coordinate
                relative to a user-specified center. 
    @author     Jordan Kochavi
    @date       March 3, 2021
    '''
    def __init__(self, pins, __dims, __center=(0,0), __res=(1,1)):
        '''
        @brief              Class object initializer 
        @details            This initializer is ran when a touch panel object
                            is instantiated. It initializes the analog MCU
                            pins that will read the panel. 
        @param pins         A tuple containing four arbitrary pin objects. For x_p, x_m,
                            y_p, y_m. The parameter must be in the format, (xp, xm, yp, ym),
                            where each element is a pyb.Pin object.
        @param __dims       A tuple containing the width and length of the 
                            touch panel. The tuple must be in float format, 
                            i.e, (176, 100), which represents the dimensions
                            of a panel, in millimeters. 
        @param __center     A tuple containing the ADC measurement that 
                            represents the center of the panel. 
                            An example of this parameter is (520,500).
        @param __res        The resolution of the touch panel. This argument
                            is passed as a tuple, containing the resolution along
                            the X and Y axes in the format, (X, Y). If this 
                            class is being used to calibrate the panel, then
                            this parameter may be left out. However, if the 
                            panel is already calibrated, then it should be
                            passed here. Resolution is the number of ADC counts
                            along an axis per millimeter. 
        '''
        ## Xp pin object
        self.xp = pins[0]
        ## Xm pin object
        self.xm = pins[1]
        ## Yp pin object
        self.yp = pins[2]
        ## Ym pin objecy
        self.ym = pins[3]
        ## Touch panel dimensions, in format (width, length), in millimeters.
        self.dims = __dims      # Units of mm
        ## Panel center, in format (x, y), in ADC counts.
        self.center = __center  # Default to 0 in X & Y if in calibration mode        
        ## Resolution, in format (x, y), in units of ADC count per millimeter.
        self.res = __res        # Default to 1 adc/mm in X & Y if in calibration mode        
        ## ADC count at far left of screen.
        self.leftBound = -1*self.dims[0]/2 
        ## ADC count at far right of screen.
        self.rightBound = self.dims[0]/2
        ## ADC count at top of screen.
        self.upBound = self.dims[1]/2
        ## ADC count at bottom of screen.
        self.downBound = -1*self.dims[1]/2
    
    def calculate(self, raw, center, res, sign=1):
        '''
        @brief              Method that converts an ADC reading to distance, in mm.
        @details            This method is called by the readX(), readY() and read()
                            methods to convert an ADC measurement to a distance
                            in millimeters. It compares the ADC measurement to 
                            the panel center using the center parameter, and then
                            converts to millimeters using the resolution parameter.
        @param raw          An integer containing the raw ADC measurement. This
                            can be either the X or Y measurement.
        @param center       An integer containing the ADC measurement that corresponds
                            to the center of the panel on a particular axis. In
                            other words, the user can pass the integer, 2100, as
                            a paramter, which can represent the center reading on the 
                            X axis. Or, they can specify the index of the tuple
                            containing the center coordinates, like center[0] or
                            center[1].
        @param res          An integer containing the resolution of the panel
                            on a particular axis. Resolution represents the number
                            of ADC readings per millimeter on a particular axis.
                            As determined by the calibration tool, the x and y 
                            axes carry different resolutions. 
        @param sign         Multiplier coefficient that can either be 1 or -1.
                            This parameter is used to force the measurement to
                            scale the desired axis differently from the raw 
                            ADC measurements. Multiplying the measurement by -1
                            can invert the positive qudrants on the touch panel
                            to create a differently-oriented set of axes.
        @return             An integer containing the converted ADC measurement,
                            in millimeters.
        '''
        val = sign*(raw - center)/res # Subract center from raw and divide by resolution
        return val                    # Return the calculated value 
    def readY(self):
        '''
        @brief              Method that reads the X coordinate of pressed location.
        @details            After initializing the appropriate pins, this method
                            measures the y_p pin to scan the X axis. Then, it 
                            converts it to a millimeter distance using the method,
                            calculate()
        @return             An integer containing the X coordinate, in millimeters,
                            relative to the center of the touch panel.
        '''
        self.xp.init(mode = pyb.Pin.OUT_PP, value = 1)                         # Initialize in push-pull mode, assert HIGH
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)                         # Initialize in push-pull mode, assert LOW
        self.ym.init(mode = pyb.Pin.IN)                                        # Initialize as input for measuring 
        self.yp.init(mode = pyb.Pin.IN)                                        # Initialize as input before floating
        self.yp.low()                                                          # Assert low to float yp pin
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.ym)                                           # Create ADC object to read ym voltage
        return self.calculate(ADCreader.read(), self.center[1], self.res[1],-1)# Convert to mm and return
    def readX(self):
        '''
        @brief              Method that reads the Y coordinate of pressed location.
        @details            After initializing the appropriate pins, this method
                            measures the x_p pin to scan the X axis. Then, it 
                            converts it to a millimeter distance using the method,
                            calculate()
        @return             An integer containing the Y coordinate, in millimeters,
                            relative to the center of the touch panel.
        '''
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)                         # Initialzie in push-pull mode, assert HIGH
        self.ym.init(mode = pyb.Pin.OUT_PP, value = 0)                         # Initialize in push-pull mode, assert LOW
        self.xm.init(mode = pyb.Pin.IN)                                        # Initialize as input for measuring
        self.xp.init(mode = pyb.Pin.IN)                                        # Initialize as input before floating
        self.xp.low()                                                          # Assert low to float xp pin
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.xp)                                           # Create ADC object to read xm voltage
        return self.calculate(ADCreader.read(), self.center[0], self.res[0])   # Convert to mm and return
    def readZ(self):
        '''
        @brief              Method that checks if the panel was pressed.
        @details            After initializing the appropriate pins, this method
                            measures the voltage from ym to determine if the panel
                            was pressed. If the panel is not pressed, ym will read
                            HIGH, and when the panel is pressed, it will read some
                            value between LOW and HIGH. 
        @return             True if the panel is pressed, False if the panel is
                            not pressed.
        '''
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)                         # Initialize in push-pull mode, assert HIGH
        self.ym.init(mode = pyb.Pin.IN)                                        # Initialize as input for measuring
        self.xp.init(mode = pyb.Pin.IN)                                        # Initialize as input before floating       
        self.xp.low()                                                          # Assert low to float xp pin
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)                         # Initialize in push-pull mode, assert LOW
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.ym)                                           # Create ADC object to read xm voltage
        if ADCreader.read() < 4000:                                            # If the measured voltage from ym is less than HIGH...
            return True                                                        #     Return True (4000 is an arbitrary threshold around 4096)
        return False                                                           # Otherwise, return False 
    def readRaw(self):
        '''
        @brief              Method that returns the raw ADC measurements for 
                            the X, Y, and Z scans. 
        @details            This method may be called on its own for calibration
                            purposes, as it doesn't include any calculations and 
                            conversions to millimeters. It simply returns the 
                            raw ADC values at the measured coordinates. 
        @return             A tuple, containing the raw ADC measurements in the
                            format, (x, y, z). z will still be returned
                            as a boolean, rather than an integer.
        '''
        self.xp.init(mode = pyb.Pin.OUT_PP, value = 1)                         # Initialize in push-pull mode, assert HIGH
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)                         # Initialize in push-pull mode, assert LOW
        self.ym.init(mode = pyb.Pin.IN)                                        # Initialize as input for measuring 
        self.yp.init(mode = pyb.Pin.IN)                                        # Initialize as input before floating
        self.yp.low()                                                          # Assert low to float yp pin
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.ym)                                           # Create ADC object to read ym voltage
        x = ADCreader.read()                                                   # Read the ADC object
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)                         # Initialize in push-pull mode, assert HIGH
        self.xp.init(mode = pyb.Pin.IN)                                        # Initialize as input before floating       
        self.xp.low()                                                          # Assert low to float xp pin
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.ym)                                           # Create ADC object to read xm voltage
        z = False                                                              # Init to False 
        if ADCreader.read() < 4000:                                            # If Z falls below threshold... 
            z = True                                                           #     We have a press!  
        self.ym.init(mode = pyb.Pin.OUT_PP, value = 0)                         # Initialize in push-pull mode, assert LOW
        self.xm.init(mode = pyb.Pin.IN)                                        # Initialize as input for measuring
        utime.sleep_us(4)                                                      # Delay 4 us after energizing resistor dividers
        ADCreader = pyb.ADC(self.xp)                                           # Create ADC object to read xm voltage
        y = ADCreader.read()                                                   # Read the ADC object
        return (x, y, z)                                                       # Return measurements in tuple
    def read(self):
        '''
        @brief              Method that returns the coordinates of the pressed
                            location, in millimeters. 
        @details            This method first uses the readRaw() method, and 
                            then calculates the measurement to millimeters using
                            the calculate() method. 
        @return             An tuple, containing the position measurements in 
                            the format, (x, y, z). z will still be a boolean,
                            rather than an integer. 
        '''
        rawVal = self.readRaw()                                                # Obtain raw measurements
        x = self.calculate(rawVal[1],self.center[0],self.res[0])               # Convert X coordinate to mm 
        y = self.calculate(rawVal[0],self.center[1],self.res[1],-1)            # Convert Y coordinate to mm
        return(x, y, rawVal[2])                                                # Return in tuple format
        
if __name__== '__main__':                                                      # Test code...
    ## @brief Hardware pin attached to panel pin X+
    xp = pyb.Pin(pyb.Pin.board.PA7)
    ## @brief Hardware pin attached to panel pin X-
    xm = pyb.Pin(pyb.Pin.board.PA1)
    ## @brief Hardware pin attached to panel pin Y+
    yp = pyb.Pin(pyb.Pin.board.PA6)
    ## @brief Hardware pin attached to panel pin Y-
    ym = pyb.Pin(pyb.Pin.board.PA0)
    dimensions = (176,100)
    center = (1997, 2080) # Obtained from calibration script
    resolution = (20,32)  # Obtained from calibration script
    panel = touch_panel((xp, xm, yp, ym),dimensions,center,resolution)
    # Test code for measuring time elapsed reading the signals
    '''
    elapsedTime = 0
    for k in range(100):
        start_time = utime.ticks_us()
        vals = panel.read()
        end_time = utime.ticks_us()
        inc = end_time-start_time
        elapsedTime += inc
    elapsedTime /= 100
    print("Average time to read all 3 values [us] "+str(elapsedTime))
    
    '''
    # Test code for outputting a constant stream of pressed coordinates.
    while True:
        try:
            if panel.readZ():
                coordinates = panel.read()
                print("X: "+str(coordinates[0])+", Y: "+str(coordinates[1]))
                #print(coordinates)
                utime.sleep_ms(100)
        except KeyboardInterrupt:
            break
    
    
    
    
    
    