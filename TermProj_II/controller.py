'''
@file controller.py
@brief      Contains the class and methods for implementing a full-state 
            feedback controller.
@details    This class works in conjunction with other sensor and feedback
            modules to implement a full-state feedback controller. The class
            is specific to the ball and platform assembly of the ME 405 term
            project, and uses the DVR8447 motor driver class, the EncoderDriver
            class, and the touch_panel class. 
@author     Jordan Kochavi
@author     Giselle Morales
@date       March 15, 2021 Original file.
'''
## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the module used to measure time.
import utime

class fullState:
    '''
    @brief      Class object of full-state feedback controller for the ball 
                and platform assembly.
    @details    This class implements a full-state feedback controller that
                is specific to the ball and platform assembly in the ME 405 
                term project. The system is attributed a motor driver object,
                two encoder objects, and a touch panel object, just as it is
                assembled in real-life. It has several methods that interface
                with each module to aquire data and feedback about the motion
                of the ball and platform. The update() method calculates new
                motor duty cycles from position and velocity feedback.
    @author     Giselle Morales
    @author     Jordan Kochavi
    @date       March 15, 2021
    '''
    def __init__(self, panel, motorDriver, encoder1, encoder2, motorParams):
        '''
        @brief   Class constructor.
        @details This function creates a controller object based on existing
                 motor driver, encoder, and touch panel objects. In addition,
                 it receives the physical parameters of the motor in the form
                 of a tuple.
        @param panel The touch panel object used to measure the position and 
                 velocity of the ball.
        @param motorDriver A motor driver object.
        @param encoder1 The encoder object attached to motor 1.
        @param encoder2 The encoder object attached to motor 2.
        @param motorParams A tuple containing the physical parameters of the 
                 motor, in the format (R, Vdc, Kt)
        ''' 
        ## The controller's touch panel attribute acts as a position and velocity sensor for the ball.
        self.ball_sensor = panel    
        ## The encoder object attached to motor 1.          
        self.motor1_sensor = encoder1
        ## The encoder object attached to motor 2.
        self.motor2_sensor = encoder2
        ## The motor driver object that controls motor 1 and motor 2.
        self.driver = motorDriver
        self.driver.motor1.setParameters(motorParams[0], motorParams[1], motorParams[2]) # Assign the motor parameters to motor 1.
        self.driver.motor2.setParameters(motorParams[0], motorParams[1], motorParams[2]) # Assign the motor parameters to motor 2.
        '''
        @brief   A list that keeps track of the time in between ball measurements.
        @details This variable is a two-element list. The control frequency
                 is assigned outside of this class as the frequency at which
                 the method, update() is called. The speed of the ball is calculated
                 as delta X / delta t, where delta t = ballTime[1] - ballTime[0].
        '''
        self.ballTime = [0,0]                # 0 index-> previous, 1 index-> current
        '''
        @brief   Stores the position of the ball, in meters.
        @details This variable is a two-element list. It contains the x and y
                 position of the ball in the format [x, y], with units of meters. 
        '''
        self.ballPos = [0,0]                 # [x, y]
        '''
        @brief   Stores the position of the ball at the previous time increment.
        @details This variable is used to calculate the ball's velocity. The 
                 speed of the ball is calculated as delta X / delta t, where
                 delta X = ballPos[0] - prevBallPos[0], and delta Y = ballPos[1] 
                 - prevBallPos[1].
        '''
        self.prevBallPos = self.ballPos      # Position at last time increment
        '''
        @brief   Stores the velocity of the ball.
        @details This variable is a two-element list. It stores the current
                 velocity of the ball in the format [Vx, Vy], where Vx and Vy
                 carry units of m/s. 
        '''
        self.ballVel = [0,0]
        '''
        @brief   A list that keeps track of the time in between motor measurements.
        @details This variable is a two-element list. The control frequency
                 is assigned outside of this class as the frequency at which
                 the method, update() is called. The speed of the motor is calculated
                 as delta theta / delta t, where delta t = motorTime[1] - motorTime[0].
        '''
        self.motorTime = [0,0]
        '''
        @brief   Stores the angle of the motors, in radians.
        @details This variable is a two-element list. It contains the angular
                 positions of both motors in the format [motor 1, motor 2]. 
                 A positive position indicates counter-clockwise motion, and 
                 a negative position indicates clockwise motion.
        '''
        self.motorPos = [0,0]              # [motor 1, motor 2]
        '''
        @brief   Stores the angles of the motors at the previous time increment.
        @details This variable is used to calculate the motors' velocities. The 
                 speed of the motor is calculated as delta theta / delta t, where
                 delta theta 1 = motorPos[0] - prevMotorPos[0], and delta theta 2 
                 = motorPos[1] - prevMotorPos[1].
        '''
        self.prevMotorPos = self.motorPos
        '''
        @brief   Stores the velocities of the motors.
        @details This variable is a two-element list. It stores the current
                 velocities of the motors in the format [w1, w2], where w1 and w2
                 carry units of rad/s. 
        '''
        self.motorVels = [0,0]             # [motor 1, motor 2]
        ## Radius of the motor arm, in meters.
        self.r_m = 0.06 # m Motor arm length
        ## Length of connecting rod, in meters.
        self.l_r = 0.05 # m Connecting rod length
        ## External interrupt object attached to blue user button. Used for motor fault correction.
        self.faultInt = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.button_isr) # Attach interrupt to fault pin
    
    def button_isr(self, param):
        '''
        @brief   ISR callback for when the blue user button is pressed.
        @details In the event of a motor fault, the serial monitor will alert
                 the user when the motor driver chip is no longer in a state of
                 fault. In order to return the motors to normal operation, the
                 user must clear the fault by pressing the blue user button.
        '''
        if self.driver.fault_OK():    # Check the DRV8847 fault state. If it's ok...
            self.driver.fault = False #    Then lower the fault flag
            self.driver.enable()      #    Re-enable chip to resume operation 
    
    def getBallPos(self):
        '''
        @brief   Function that gets the current position of the ball, in meters.
        @details This method returns the position of the ball in the format,
                 [x, y], where x and y are in meters. The position of the ball
                 is with respect to the center of the panel. If the ball is
                 not in contact with the screen, then this function returns None.
        @return A list containing the current position of the ball.
        @return None Only if there is nothing touching the screen.
        '''
        ballPos = [0,0]                          # Initialize to zero
        if self.ball_sensor.readZ():             # If the screen is being touched...
            ballPos_mm = self.ball_sensor.read() #     Returns in mm
            ballPos[0] = ballPos_mm[0]/1000      #     Convert to m
            ballPos[1] = ballPos_mm[1]/1000      #     Convert to m
            return ballPos                       #     Return the position
        else:                                    # Otherwise...
            ballPos = None                       #     Return None

    def getBallVel(self):
        '''
        @brief   Function that gets the current velocity of the ball, in m/s.
        @details This function calculates the velocity of the ball based on its
                 current position, its previous position, and the time interval
                 between measurements. The velocity of the ball is calculated as
                 delta S / delta t, where S may be the ball's position along 
                 the X or Y axis. The time interval between position measurements
                 is obtained using the utime function, ticks_us(). Since this 
                 is used to calculate the time interval, in microseconds, the 
                 calculated velocity is multiplied by 10^6 to convert to seconds.
        @return  A list containing the velocity of the ball, in the format, [Vx, Vy]
                 in meters per second.
        '''
        ballVel = [0,0]                          # Initialize to zero
        self.ballTime[1] = utime.ticks_us()      # Returns microseconds
        # Convert to m/s
        ballVel[0] = 1000000*(self.ballPos[0] - self.prevBallPos[0])/(self.ballTime[1] - self.ballTime[0]) # delta X / delta t
        ballVel[1] = 1000000*(self.ballPos[1] - self.prevBallPos[1])/(self.ballTime[1] - self.ballTime[0]) # detla Y / delta t     
        return ballVel                           # Return velocity

    def getMotorAngles(self):
        '''
        @brief   Function that gets the current angular position of both motors.
        @details This method reads the current encoder counts to calculate the 
                 absolute position of motor 1 and motor 2. A positve angular
                 position indicates counter-clockwise rotation, and a negative
                 angular position indicates clockwise rotation.
        @return  A tuple containing the angular positions of motor 1 and motor 2
                 in the format, (theta 1, theta 2), in degrees.
        '''
        self.motor1_sensor.update()                         # Update motor 1's encoder count
        self.motor2_sensor.update()                         # Update motor 2's encoder count
        position1 = self.motor1_sensor.get_position()       # Get the current position of motor 1 
        position1 = self.motor1_sensor.ticks_deg(position1) # Convert to degrees       
        position2 = self.motor2_sensor.get_position()       # Get the current position of motor 2
        position2 = self.motor2_sensor.ticks_deg(position2) # Convert to degrees
        return (position1, position2)                       # Return positions in tuple

    def getMotorVels(self):
        '''
        @brief   Function that calculates the angular velocity of both motors.
        @details This method uses the current motor positions and their previous
                 positions at the last time increment to calculate their angular
                 velocities. A positive angular velocity indicates counter-
                 clockwise rotation, and a negative angular velocity indicates
                 clockwise rotation. 
        @return  A tuple containing both motor velocities, in degrees per second.
        '''
        self.motorTime[1] = utime.ticks_us()                # Return in microseconds
        # Calculate in degrees per second
        motor1Vel = 1000000*(self.motorPos[0] - self.prevMotorPos[0])/(self.motorTime[1] - self.motorTime[0]) # delta X / delta t
        motor2Vel = 1000000*(self.motorPos[1] - self.prevMotorPos[1])/(self.motorTime[1] - self.motorTime[0]) # detla Y / delta t
        return (motor1Vel, motor2Vel)                       # Return velocities in tuple
        
    def setKmatrix(self,motor,k):
        '''
        @brief   Function that sets the gain matrix for a particular motor object.
        @details In tuning the system, it is helpful to apply different gain
                 matrices to each motor. It makes sense that the motors should
                 have different gains because the touch panel is not square.
                 The panel is more narrow along the y-axis. The position of the 
                 ball in the y-direction is affected by motor 1. Since this 
                 direction allows for less motion, it makes sense that we should
                 want motor 1 to actuate more extremely, since there is less 
                 room and time for the ball's motion to compensate. Note that 
                 these gain matrices are attributes of the controller, not of
                 a motor object. 
        @param motor A motor object.
        @param k A 1x4 K matrix containing gain values.
        '''
        if motor == 1:       # If motor 1 is passed...
            self.K1 = k      #    Then assign K matrix to motor 1
        elif motor == 2:     # Else if motor 2 is passed...
            self.K2 = k      #    Then assign K matrix to motor 2 
        else:                # If neither a 1 nor 2 were passed...
            raise TypeError  #    Then raise a TypeError
    
    def transform(self, angles):
        '''
        @brief   This function converts the angular position or angular
                 velocity of a motor to the corresponding platform angle. 
        @details The calculated angle of a motor is a function of the pulley ratio
                 between the encoder and the motor, and the calculated platform
                 angle is a function of the motor angle and the ratio between
                 the motor arm and connecting rod lengths. These conversions 
                 are encapsulated in this method. The pulley ratio between the 
                 motor and encoder, as well as the other discrepancies between
                 the motor angle and platform angle, were calculated using an
                 external inclinometer in an iPhone app. It was determined that
                 the motor angle, as measured by the method getMotorAngles() 
                 is linearly related to the actual angle of the platform. A 
                 series of measurements were taken, and curve fit was applied
                 that calculates the actual angle of the platform from the 
                 measured angle of the motor. The slope of the curve fit is 
                 0.413 deg/deg.    
        @param angles A list or tuple containing the angular positions or velocities
                 to be transformed. Must be in the format, [motor 1, motor 2]. 
        @return A list containing the transformed angles, in radians OR rad/s,
                 depending on if a position or velocity was passed.
        '''
        transformAngles = [0,0]                             # Initialize to zero
        transformAngles[0] = -1*angles[0]*self.r_m/self.l_r # Apply arm and connecting rod ratio
        transformAngles[1] = -1*angles[1]*self.r_m/self.l_r # Apply arm and connecting rod ratio 
        transformAngles[0] = transformAngles[0]*0.413       # Apply curve fit
        transformAngles[1] = transformAngles[1]*0.413       # Apply curve fit
        transformAngles[0] *= 3.1415926/180                 # Convert to radians
        transformAngles[1] *= 3.1415926/180                 # Convert to radians
        return transformAngles                              # In radians
    
    def nearEdge(self, position, factor1, factor2):
        '''
        @brief   Method that determines if the ball is nearing the edge of the 
                 touch panel.
        @details This function is useful in testing, and is used to alert the
                 controller when the ball is approaching an edge of the screen.
                 If the ball is approaching the top of the panel, a positive 
                 flag is raised for motor1, as motor1 controls the vertical
                 position of the ball. Similarly, a negative flag is raised if
                 the ball is approaching the bottom of the panel. If the ball
                 is approaching the right edge of the panel, a positive flag
                 is raised for motor 2, and a negative flag is raised if it
                 is approaching the left edge. Finally, a general flag is 
                 raised if the ball is approaching any edge.
        @param position A list or tuple containing the current position of the ball
                 in the format, [x, y] in meters. 
        @param factor1 A decimal percentage of the vertical screen dimension
                 that constitutes when the ball is "near" that edge. For example,
                 entering 0.9 will alert the controller when ball's y-coordinate
                 exceeds 90% of the vertical screen dimension (could be approaching
                 the top or bottom edge of the panel).
        @param factor2 A decimal percentage of the horizontal screen dimension
                 that constitutes when the ball is "near" that edge. For example,
                 entering a 0.8 will alert the controller when the ball's x-
                 coordinate exceeds 80% of the horizontal screen dimension (could 
                 be approaching the left or right edge of the panel)
        @return  A tuple containing all three flags.
        '''
        factor1 /= 1000  # Includes conversion to meters
        factor2 /= 1000  # Includes conversion to meters
        motor1 = 0       # Initialize to zero
        motor2 = 0       # Initialize to zero
        edge = False     # Initialize to false 
        if position[0] < factor2*self.ball_sensor.leftBound:     # If the X coordinate is near the left edge...
            motor2 = -1                                          #   Then raise the negative flag for motor 2
            edge = True                                          #   Raise the general edge flag 
        elif position[0] > factor2*self.ball_sensor.rightBound:  # Else if the X coordinate is near the right edge...
            motor2 = 1                                           #   Then raise the positive flag for motor 2 
            edge = True                                          #   Raise the general edge flag
        else:                                                    # Otherwise...
            motor2 = 0                                           #   Leave the flag as 0
        if position[1] < factor1*self.ball_sensor.downBound:     # If the Y coordinate is near the bottom edge...
            motor1 = -1                                          #   Raise the negative flag for motor 1 
            edge = True                                          #   Raise the general edge flag 
        elif position[1] > factor1*self.ball_sensor.upBound:     # Else if the Y coordinate is near the top edge...
            motor1 = 1                                           #   Raise the positive flag for motor 1
            edge = True                                          #   Raise the general edge flag
        else:                                                    # Otherwise...
            motor1 = 0                                           #   Leave the flag as 0
        return (edge, motor1, motor2)                            # Return a tuple of all three flags. 
        
    def update(self):        
        '''
        @brief   Controller method that updates the duty cycles of both motors. 
        @details This method aquires feedback from the encoders and the touch
                 panel to calculate the positions and velocities of the ball
                 and platform. Then, it calculates a torque value for each motor
                 using their respective gain matrix, and sends a new set of 
                 duty cycles to the motor driver chip. This function must be 
                 called periodically at a fixed interval.
        '''
        self.ballPos = self.getBallPos()                         # Get the current ball position
        if self.ballPos != None:                                 # If the ball is actually on the screen...
            self.ballVel = self.getBallVel()                     #    Then get its velocity
        else:                                                    # Otherwise...
            self.ballVel = [0,0]                                 #    Leave it at zero
            self.ballPos = [0,0]                                 #    Leave the position at zero too
        self.motorPos = self.getMotorAngles()                    # Get the current motor angles
        self.motorVels = self.getMotorVels()                     # Get the motor velocities
        platformAngles = self.transform(self.motorPos)           # Convert the motor angles to platform angles
        platformVels = self.transform(self.motorVels)            # Convert the motor speeds to platform speeds
        if not self.driver.fault:                                # If the motor driver chip is not in fault...
            # Calculate the torque for motor 1
            T1 = 1*self.K1[0]*self.ballVel[1]-self.K1[1]*platformVels[0]+self.K1[2]*self.ballPos[1]-self.K1[3]*platformAngles[0]
            duty1 = T1*self.driver.motor1.gain                   # Calculate a duty cycle percentage for motor1
            self.driver.motor1.set_duty(duty1)                   # Set the duty cycle
            # Calculate the torque for motor 2
            T2 = 1*self.K2[0]*self.ballVel[0]-self.K2[1]*platformVels[1]+self.K2[2]*self.ballPos[0]-self.K2[3]*platformAngles[1]
            duty2 = T2*self.driver.motor2.gain                   # Calculate a duty cycle percentage for motor2            
            self.driver.motor2.set_duty(duty2)                   # Set the duty cycle
        self.prevBallPos = self.ballPos                          # Reset for next time interval
        self.prevMotorPos = self.motorPos                        # Reset for next time interval
        self.motorTime[0] = self.motorTime[1]                    # Reset for next time interval
        self.ballTime[0] = self.ballTime[1]                      # Reset for next time interval
        return (duty1, duty2, self.ballPos[0], self.ballPos[1])  # Return tuple for plotting