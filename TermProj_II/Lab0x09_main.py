'''
@file Lab0x09_main.py

@brief A test script with an FSM for testing the controller class, and a 
reflection on the controller design process.

@details This test file instantiates a fullState controller object and updates 
it in an FSM. This test script is used to define the gain matrices and create 
all required pin objects for the system, as well as define any other relevant 
system parameters, such as the touch panel dimensions.

Our term project video is visible at <a href="https://youtu.be/g5vki9PibzI"><b>this Youtube video</b></a>.
The writeup below references sections in the video and discusses our testing
results. 

@section sec1 Calibrating Platform Angles
The first thing we did is make sure that the encoders could be used to correctly
calculate the angle of the platform. To do this, we compared their output to 
the results of an inclinometer app. The phone was placed on the touch panel, 
and the platform angle was measured along both axes in increments of two degrees.
This angle was compared to the calculated motor angle, which only accounted for
the ratio between the motor arm and connecting rod lengths. What was not yet 
accounted for were the pulley ratio between the encoder and the motor, and any
other dimensions that affected the platform angle. The plot below compares the
actual platform angle with the calculated angles.

<img src="https://bitbucket.org/gmoral05/me405lab_group/raw/115791e73fba07a5f545199798928475ed3983dc/TermProj_II/angleCalcs.png" alt="Angle Plot" width="500">

As shown in the plot above, both motors have a nearly identical linear 
relationship with the angle of the platform. A linear curve fit was applied to
both motors, and their slopes were averages to calculate a single coefficient.
Multiplying the calculated motor angle by 0.413 (deg/deg), which is the average of the 
two slopes, allowed us to acheive an accurate calculation of the platform angle
from both motors.

@section sec2 Controller Design
To design the controller, we tested the response of the system to different 
forced inputs, without any controller input. This was done mostly to asses the
sign values of the various angles and velocities. The picture below shows the 
positive directions all the various axes on the platform. 

<img src="https://bitbucket.org/gmoral05/me405lab_group/raw/c01c34a9e4014ef5f9b8aa5ebd3a7dd7e5fd5c4d/TermProj_II/annotatedPlatform.png" alt="Angle Plot" width="500"> 

Shown above, when Motor 1 rotates counter-clockwise, the platform rotates 
clockwise about the x-axis. For the controller, we established a positive datum
at the counter-clockwise rotation of Motor 1 and Motor 2.

When determining the equation for the torque and duty cycle output of the motor,
we considered how we wanted the motor to respond to a particular value of a state
variable. For example, if the platform were rotated clockwise about the X axis,
we would want Motor 1 to spin clockwise to bring the platform back to neutral.
Since the clockwise rotation of the platform is a negative angle, we want a 
negative duty cycle for a negative platform angle.

Now for the ball position, as indicated in the picture above, the positive 
quadrant of the touch panel is in its top right corner. If the ball were to be
placed in the positive y half of the panel, we would want the platform to 
rotate counter-clockwise to bring the ball back to the center. To acheive 
counter-clockwise rotation for the motor, we need a clockwise rotation for 
Motor 1. In other words, we want the controller to assign a negative duty
cycle for a positive y position. 

This is why in our torque equation, the angular terms carry opposite signs as
the ball positional terms. The same logic is applied Motor 2 for the other
platform angle, and the motion of the ball along the X axis.

We tested our duty cycle equation by gently tilting the platform to different
angles. As shown in our video, tilting the platform clockwise calculates
a negative duty cycle for Motor 1, which means that it wants to rotate clockwise
to pull the the platform counter-clockwise.

@section sec3 Analytical Gains
A discussion on the derivation of the analytical gains for this lab can be
viewed on <a href="CL_calcs.html"><b>this page</b></a>, which is an annotated
MATLAB script, with hand calculations embedded. 

@section sec5 Tuning Controller & Final Results
After lots of tuning and modifications, we couldn't get the platform to move
the ball back to the center when it was displaced a certain distance. Also, we
had a lot of trouble working with the velocity of the ball. Whenever the ball
would lose contact with the platform, like when it leaves the active area
of the touch panel, the position of the ball would immedietely revert to [0,0], 
which means that the position is changing from something really big to zero,
which causes a big spike in the calculated velocity. This makes it difficult
to handle extreme cases of the ball's motion on the platform because when the 
velocity spikes, the platform would jerk pretty abruptly. 

We tried to correct this issue with the method nearEdge(), which is an edge 
detection function, to try to act preemtively if the ball gets too close to 
an edge of the active area. In the update() method, we first checked if the 
ball was approaching an edge, and if it was, we would bypass the torque/duty
cycle calculation and just apply a fixed jolt to push the ball away from the 
edge to ensure that it doesn't fall off. This didn't work because it applied
the same jolt regardless of how fast the ball was moving. It worked a little
bit when the ball was rolling around the platform very quickly because the 
jolt was powerful enough to push it in the opposite direction, but when the 
ball was rolling very slowly, the same jolt would shoot the ball back very
quickly and make it fall off the opposite edge. Thus, we definitely
needed the proportional gain for the speed of the ball. There must be a better
way to account for the extreme cases on the platform, but for now we had much
better results focussing on just holding the ball stationary.

We tuned the system to hold the ball in place wherever it's placed on the touch
panel. The platform is still doing some balancing, but just isn't trying to 
relocate the ball to the center of the platform. We used the following gain
matrices for Motor 1 and Motor 2, respectively.

* K1 = [-0.2, -0.5, -50, -40]
* K2 = [-0.2, -0.5, -25, -40]
  
Motor 1 controls the y-position of the ball. The touch panel is much more 
narrow in the y-direction than the x-direction, which means that the ball 
doesn't have a very big range of travel along the y-axis as it does along the
x-axis. To account for this, we increased K3 on gain matrix K1, which corresponds
to the gain value for the y-position of the ball. It makes sense that the y
direction should be more sensative to position, since we don't want the ball
to travel as far in the y-direction. The result is shown in our video.

As shown in our video, the controller does a good job of holding the ball in
place wherever it is placed on the panel. To acheive this, we first applied
the initial conidition of:
    
* No ball
* Zero velocities
* Zero initial platform angle
* Applying an impulse to the platform

A plot of this scenario being applied is visible in the figure below. This
test is also visible in our term project video. 

<img src="https://bitbucket.org/gmoral05/me405lab_group/raw/115791e73fba07a5f545199798928475ed3983dc/TermProj_II/platformPlot.png" alt="Angle Plot" width="500">

When the platform is pulled to a certain angle, the controller compensates
for its displacement and returns it to a flat position. However, when we added
the ball, the platform jolted too powerfully, so we had to reduce K2 and K4 
until the system stabilized more. After we acheieved a more stable platform, 
we tried the initial conditions of:

* Non-zero X0
* Non-zero Y0
* Zero initial platform angle
* Zero initial velocities

Using this test script, we printed the motor duty cycles, along with the
ball's position, to the serial monitor, and saved the results in a CSV file.
The plot below shows the response of the platform and the ball to one of these
tests.

<img src="https://bitbucket.org/gmoral05/me405lab_group/raw/115791e73fba07a5f545199798928475ed3983dc/TermProj_II/motor1vT.png" alt="Angle Plot" width="500">

The plot above shows the ball being placed at a Y-coordinate of around 30 mm from
the center of the platform. The controller initially tries to compensate for the 
disturbance by applying a negative duty cycle (it was previously at zero, as the
platform was level). As discussed earlier, it makes sense that the controller
would decide on a negative duty cycle for Motor 1 because the ball's position
was positive. After the controller applies its initial duty cycle, the ball
began to shift a little bit, and the plot shows its y-position decreasing. 
This makes sense because Motor 1 spinning clockwise (negative duty cycle) should
made the platform rotate counter-clockwise, which made the ball tip in the 
negative y direction. Next, the controller compensates for the ball's change
in motion by decreasing its duty cycle until the ball's position steadied off.
This explains why the shapes of both curves look inverted to each other.

<img src="https://bitbucket.org/gmoral05/me405lab_group/raw/115791e73fba07a5f545199798928475ed3983dc/TermProj_II/motor2vT.png" alt="Angle Plot" width="500">

Similarly to the Motor 1 plot, the Motor 2 plot behaves similarly. We also 
see an inverted shape between the Motor 2 duty cycle and the ball's position 
along the X axis. However, in this case, the ball's starting X position was 
negative, which means it was closer to the left edge of the platform. When
the ball's X position is negative, the ball's force of gravity wants to make 
the platform tip counter-clockwise, so we want the controller to tip the 
platform clockwise to compensate for this. In order for Motor 2 to make the
platform tip clockwise, Motor 2 needs to rotate counter clockwise, which means
the controller should assign it a positive duty cycle. As shown in the plot
above, the duty cycle jumps to a positive value. This begins to tip the 
platform tip clockwise a little bit, which makes the ball start to tip toward
the origin, which is why we see a decrease in the ball's X coordinate. Then,
we see a positive increase in the duty cycle until the X coordinate levels 
off. 

Finally, we applied the initial condition of:
    
* Zero initial ball position
* Zero initial platform angle
* Zero velocities
* Applying an impulse to the ball

As the video shows, it at first is hard to tell if the controller is making the
ball return to the origin, or if it was just from the natural stiffness of the 
U-joint. However, after a couple bigger pushes, the system can definitely be 
observed responding. It successfully stops the ball until it was pushed hard
enough to shoot it off one of the edges. 

@section sec4 Comparing Analytical Gains With Testing
We first tested the system using the gains derived in the hand calculations 
without the ball. Without the ball, we couldn't get the platform to response
to an impulse for much less than a gain of 50 (much different from 14). 
However, the general trend of K2 >> K1 remained throughout our tuning process.
The analytical calculations informed us that we should ideally choose a much
larger gain value for the platform angular position than for its angular 
velocity. 

Similarly, the gains derivated in the MATLAB script didn't produce much of a
response from the platform. We would delicately balance the ball in the center,
but the slightest movement would push it off the platform, and the controller
wouldn't do anthing to compensate. To summarize, a comparison of the analytical
gain values to the ones that produced the best results are shown below:
    
* Analytical:
    * K = [-3.28, -0.29, -9.09, -3.99]
* Best Results:
    * Motor 1: [-0.2, -0.5, -50, -40]
    * Motor 2: [-0.2, -0.5, -25, -40]

@author     Jordan Kochavi
@author     Giselle Morales
@date       March 15, 2021 Original file.
'''
## @brief Import the py-board module.
import pyb
## @brief Import the motor driver class.
from MotorDriver import DVR8847
## @brief Import the encoder driver class.
from encoder import EncoderDriver
## @brief Import the touch panel module.
from touchPanel import touch_panel
## @brief Import the controller module.
from controller import fullState
## @brief Import the time module.
import utime

PA15 = pyb.Pin(pyb.Pin.board.PA15) # For nSleep
PB2 = pyb.Pin(pyb.Pin.board.PB2)   # For nFault
PB4 = pyb.Pin(pyb.Pin.board.PB4)   # For IN1
PB5 = pyb.Pin(pyb.Pin.board.PB5)   # For IN2
PB0 = pyb.Pin(pyb.Pin.board.PB0)   # For IN3
PB1 = pyb.Pin(pyb.Pin.board.PB1)   # For IN4
PB6 = pyb.Pin(pyb.Pin.board.PB6)   # Encoder pin motor 1
PB7 = pyb.Pin(pyb.Pin.board.PB7)   # Encoder pin motor 1
PC6 = pyb.Pin(pyb.Pin.board.PC6)   # Encoder pin motor 2
PC7 = pyb.Pin(pyb.Pin.board.PC7)   # Encoder pin motor 2
PA7 = pyb.Pin(pyb.Pin.board.PA7)   # X+
PA1 = pyb.Pin(pyb.Pin.board.PA1)   # X- 
PA6 = pyb.Pin(pyb.Pin.board.PA6)   # Y+
PA0 = pyb.Pin(pyb.Pin.board.PA0)   # Y-

## The motor load resistance, in ohms.
R = 2.21  # ohms
## The motor nominal voltage, in Volts.
Vdc = 12  # Volts
## The motor torque constant, in mN-m/A
Kt = 13.8 # mN-m/A
## The gain matrix applied to motor 1, in the format of [K1, K2, K3, K4]
K1 = [-0.2, -0.5, -50, -40]
## The gain matrix applied to motor 2, in the format of [K1, K2, K3, K4]
K2 = [-0.2, -0.5, -25, -40]
## A timer object for hardware timer 3.
timer3 = pyb.Timer(3)                     # Create timer object for timer hardware 3
## A tuple containing the dimensions of the active area of the touch screen.
#  It has the format, (length, width), where both dimensions are in mm.
dimensions = (176,100)
## The center of the screen, in ADC counts.
center = (1966, 2068) # Obtained from calibration script
## The resolution of the screen, in ADC counts per millimeter.
resolution = (21,34)  # Obtained from calibration script
## The touch panel object used to provide feedback to the controller.
panel = touch_panel((PA7, PA1, PA6, PA0),dimensions,center,resolution)
## The motor driver object that the controller outputs to.
motorDriver = DVR8847(PA15, PB2, PB4, PB5, PB0, PB1, timer3) # Create motor driver object
## The encoder object for motor 1 that provides feedback to the controller.
encoder1 = EncoderDriver(PB6, PB7, 4)                        # Create encoder object on timer 4
## The encoder object for motor 2 that provides feedback to the controller.
encoder2 = EncoderDriver(PC6, PC7, 8)                        # Create encoder object on timer 8
## The controller object for the ball and platform system.
system = fullState(panel, motorDriver, encoder1, encoder2, (R, Vdc, Kt))

def waitingDone(time, interval):
    '''
    @brief   A function that checks if a waiting interval has passed.
    @details This function is called in the FSM to check if it's time to update
             the controller. 
    @param time A list containing the ticks_ms() for the current time, and the
             previous time interval.
    @param interval The interval, in milliseconds.
    @return  True if the interval has passed, False if the interval has not
             yet passed.
    '''
    time[1] = utime.ticks_ms()          # Get the current timestamp, in ms
    if (time[1] - time[0]) > interval:  # If current time - previous time > interval...
        return True                     #   Return True
    else:                               # Otherwise...
        return False                    #   Return False

## A list that stores the current time stamp and previous timestamps, in the
#  format, [previous, current], in ms.
time = [0,0]   # Initialize to zero
## State variable for the FSM.
state = 0      # Initialize to zero
## Counts the time elaspsed, in milliseconds. Used for plotting.
timeCount = 0  # Initialize to zero
while True:
    if state == 0:                   # State 0 - Init
        print("Time [ms], Motor 1 Duty, Motor 2 Duty, Platform 1 Angle, Platform 2 Angle")
        system.setKmatrix(1,K1)      # Set the K matrix for motor 1
        system.setKmatrix(2,K2)      # Set the K matrix for motor 2
        system.driver.enable()       # Enable the motor driver
        utime.sleep_ms(20)           # Brief delay
        state = 1                    # Transition to state 1
    elif state == 1:                 # State 1 - Wait... 
        if waitingDone(time, 50):    # If we're done waiting 50 ms... 
            time[0] = time[1]        #    Then reset for next waiting period
            state = 2                # Transition to state 2
    elif state == 2:                 # State 2 - Update the controller 
        storedData = system.update() # Update the controller
        print(str(timeCount)+","+str(storedData[0])+","+str(storedData[1])+","+str(storedData[2])+","+str(storedData[3]))
        timeCount += 50              # Increment time count
        state = 1                    # Return to state 1 for waiting