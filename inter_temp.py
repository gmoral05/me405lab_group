'''
@file inter_temp.py
@brief      Brief doc for .py
@details    Detailed doc for .py
@author     Giselle Morales
@date       February 4th, 2020

@package    ADCAll
@brief      Brief doc for the ___ module
@details    Detailed doc for the ___ module
@author     Giselle Morales
@date       February 


'''

import pyb
import utime


# Measure the inital temperature of the microcontroller

## Object for measuring board temp
adcall= pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels

## Set-up time intervals
starttim= utime.ticks_ms()
currtim= utime.ticks_ms()

with open ('Board_temp.ext', 'w') as file:
    try:
        while True:
        # if #add conditional statment using time to keep loop running
            utime.sleep(6) #sleeps 10 s
            # Read the temp of the MCU
            temp = adcall.read_core_temp() #loop to keep updating
            print (temp)
            file.write('{:}\r\n'.format(temp))
        
    except KeyboardInterrupt:
        # Close file but keeps data
        file.close()
    
    
print ('Done. The file has been closed automatically.')





